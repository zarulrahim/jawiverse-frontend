import Head from 'next/head'
import { useState } from 'react'
import styles from '../styles/Home.module.css'

export default function Gallery() {
  const [title, setTitle] = useState("jawiverse.io")

  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <div>
          <h1>This is gallery.js</h1>
        </div>
      </main>
    </div>
  )
}

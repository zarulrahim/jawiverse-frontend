import Head from 'next/head'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
// import styles from '../styles/Home.module.css'
// import Header from "./components/Header"
import axios from 'axios';
import Web3 from 'web3';
// import '/public/pentasSmartContractAbi.json'
// import pentasContractAbi from "./public/pentasSmartContractAbi.json";
// import { useKeenSlider } from "keen-slider/react"
import Slider from 'react-slick'
import { FiShare2, FiHeart, FiSearch, FiChevronUp, FiChevronDown } from 'react-icons/fi';
import ReactPlayer from 'react-player';
import { Tab, Disclosure } from '@headlessui/react'
import { useRouter } from 'next/router';
import { observer } from 'mobx-react-lite';
import { getSnapshot } from 'mobx-state-tree';
import { mstGeneral } from '../mobx';

const Tokens = observer((props) => {
  const { query, locale, locales, defaultLocale, asPath } = useRouter();
  const router = useRouter();
  const [keyword, setKeyword] = useState("")
  const [checkedTokenTypes, setCheckedTokenTypes] = useState([])
  useEffect(() => {
    if (query !== undefined) {
      console.log("check query ===> ", query)
    }
  }, [query])
  useEffect(() => {
    mstGeneral.fetchTokens();
  }, [])

  const onCheckTokenType = (type, event, currentCheckedValues) => {
    const found = currentCheckedValues.includes(type)
    let newValues = currentCheckedValues.slice()
    if (found) {
      // setCheckedTokenTypes(arr => arr.filter(i => i !== found))
      setCheckedTokenTypes(newValues.filter(i => i !== type))
    } else {
      newValues.push(type)
      setCheckedTokenTypes(newValues)
    }
  }

  const onChangeKeyword = (event) => {
    console.log("keyword ===> ", event.target.value)
    setKeyword(event.target.value)
  }

  const onSearch = (event) => {
    if (event.key === 'Enter') {
      console.log("onSearch ===> ", keyword)
      if (keyword !== '') {
        router.query.keyword = keyword
        router.push(router)
      }
    }
  }

  useEffect(() => {
    if (router !== undefined) {
      console.log("current router ===> ", router.query)
      const params = {
        ...router.query
      }
      mstGeneral.fetchTokens(params)
    }
  }, [router])

  useEffect(() => {
    if (checkedTokenTypes.length > 0) {
      router.query.token_types = checkedTokenTypes.map(i => i).join(',')
      router.push(router)
    }
  }, [checkedTokenTypes])

  return (
    <div className='min-h-screen'>  
      {/* <div id="roadmap" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-slate-900 bg-opacity-20 py-10 ">
          <div>
            <div className="text-center">
              <h1 className="text-white text-2xl md:text-5xl">Roadmap</h1>
              <h5 className="mt-4 text-gray-400 text-xl">Good roadmap delivers good outcome!</h5>
            </div>
            <div className='px-4 sm:px-10 mt-10 flex items-start'>
              
            </div>
          </div>
        </div>
      </div> */}
      <div id="rumi" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-gray-900 bg-opacity-80 py-10 ">
          <div className='w-full max-w-7xl pt-20 px-4 md:px-0'>
            <div className='mt-0 flex justify-between items-center'>
              <div>
                <h1 className="text-white uppercase font-bold text-3xl md:text-4xl">Tokens</h1>
                <h5 className="mt-2 text-gray-400 text-lg">Jawispace token database</h5>
              </div>
              <div className='flex'>
                {/* <FiShare2 className='ml-5 text-white text-3xl' /> */}
              </div>
            </div>
            <div className='w-full mt-6 border-t border-gray-200 dark:border-gray-700'>
              <div className='grid grid-cols-1 md:grid-cols-4 gap-0 md:gap-5 mt-5'>
                <div>
                  <div className='bg-gray-800 rounded-lg'>
                    <div className='border-b border-gray-900 px-4 py-4'>
                      <div className='text-white'>Search</div>
                      <div className="relative mt-2">
                        <div className="flex text-white absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                          <FiSearch className='text-lg' />
                        </div>
                        <input onChange={onChangeKeyword} onKeyDown={onSearch} type="text" id="search-icon" className="bg-gray-600 placeholder-gray-400 border ring-1 ring-red-600 border-red-600 text-white text-sm rounded-lg focus:ring-red-600 focus:ring-1 focus:border-red-600 focus:bg-gray-700 block w-full pl-10 p-2.5" placeholder="jawi stone alif, rumi, poltron etc..."/>
                      </div>
                    </div>
                    <div>
                      <div className='px-4 py-2 text-white'>Filter By</div>
                        <div className='mt-1'>
                        <Disclosure>
                          {({ open }) => (
                            <>
                              <Disclosure.Button disabled className="flex justify-between w-full text-sm font-medium text-left text-white bg-gray-700 focus:outline-none focus-visible:ring focus-visible:ring-gray-500 focus-visible:ring-opacity-75 px-4 py-2">
                                <span>Token Type</span>
                                <FiChevronDown
                                  className={`${
                                    !open ? 'transform rotate-180' : ''
                                  } w-5 h-5 text-gray-500`}
                                />
                              </Disclosure.Button>
                              {
                                !open && (
                                  <Disclosure.Panel static className="px-4 pt-2 pb-2 text-sm text-gray-500">
                                    <div className="flex items-center my-2">
                                      <input id="checkbox-1" onClick={(e) => { onCheckTokenType("jacs", e, checkedTokenTypes) }} aria-describedby="checkbox-1" type="checkbox" className="w-4 h-4 text-red-600 bg-gray-100 rounded cursor-pointer focus:ring-offset-0 focus:ring-0" />
                                      <label htmlFor="checkbox-1" className="ml-3 text-sm font-base text-gray-900 dark:text-gray-300">Access (JACS)</label>
                                    </div>
                                    <div className="flex items-center my-2">
                                      <input id="checkbox-1" onClick={(e) => { onCheckTokenType("jast", e, checkedTokenTypes) }} aria-describedby="checkbox-1" type="checkbox" className="w-4 h-4 text-red-600 bg-gray-100 rounded cursor-pointer focus:ring-offset-0 focus:ring-0" />
                                      <label htmlFor="checkbox-1" className="ml-3 text-sm font-base text-gray-900 dark:text-gray-300">Asset (JAST)</label>
                                    </div>
                                  </Disclosure.Panel>
                                )
                              }
                            </>
                          )}
                        </Disclosure>
                        <Disclosure>
                          {({ open }) => (
                            <>
                              <Disclosure.Button disabled className="flex justify-between w-full text-sm font-medium text-left text-white bg-gray-700 focus:outline-none focus-visible:ring focus-visible:ring-gray-500 focus-visible:ring-opacity-75 px-4 py-2">
                                <span>Project</span>
                                <FiChevronDown
                                  className={`${
                                    !open ? 'transform rotate-180' : ''
                                  } w-5 h-5 text-gray-500`}
                                />
                              </Disclosure.Button>
                              {
                                !open && (
                                  <Disclosure.Panel static className="px-4 pt-2 pb-2 text-sm text-gray-500">
                                    {/* <div className="flex items-center my-2">
                                      <input id="checkbox-1" aria-describedby="checkbox-1" type="checkbox" className="w-4 h-4 text-red-600 bg-gray-100 rounded cursor-pointer focus:ring-offset-0 focus:ring-0" />
                                      <label htmlFor="checkbox-1" className="ml-3 text-sm font-base text-gray-900 dark:text-gray-300">RUMI: 3D Animated Series</label>
                                    </div> */}
                                    Coming soon
                                  </Disclosure.Panel>
                                )
                              }
                            </>
                          )}
                        </Disclosure>
                        <Disclosure>
                          {({ open }) => (
                            <>
                              <Disclosure.Button disabled className="flex justify-between w-full text-sm font-medium text-left text-white bg-gray-700 focus:outline-none focus-visible:ring focus-visible:ring-gray-500 focus-visible:ring-opacity-75 px-4 py-2">
                                <span>Rarity</span>
                                <FiChevronDown
                                  className={`${
                                    !open ? 'transform rotate-180' : ''
                                  } w-5 h-5 text-gray-500`}
                                />
                              </Disclosure.Button>
                              {
                                !open && (
                                  <Disclosure.Panel static className="px-4 pt-2 pb-2 text-sm text-gray-500">
                                    Coming soon
                                  </Disclosure.Panel>
                                )
                              }
                            </>
                          )}
                        </Disclosure>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='col-span-3 mt-4 md:mt-0'>
                  <div className='flex justify-between items-center pb-4'>
                    <div className='text-white'>
                      Showing {getSnapshot(mstGeneral)?.tokens?.length} items
                    </div>
                    {/* <div className='text-white'>
                      Sort By
                    </div> */}
                  </div>
                  <div className='grid grid-cols-2 md:grid-cols-4 gap-4'>
                  {
                    getSnapshot(mstGeneral)?.tokens?.map((item, index) => {
                      return (
                        <div key={index}>
                          <div className={`rounded-lg overflow-hidden max-w-full shadow-lg bg-white`}>
                            <img className="w-full" src={item?.poster_url} alt="" />
                            <div className="px-4 py-2">
                              <div className="text-md mb-0 line-clamp-1">{item?.title}</div>
                              {/* <p className="text-gray-700 text-base">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                                </p>*/}
                            </div>
                            <div className="px-4 pt-0 pb-2">
                              <span className="inline-block bg-gray-200 rounded-lg px-2 py-1 text-xs text-gray-700 mr-2 mb-1">{item?.token_type}</span>
                            </div>
                          </div>
                        </div>
                      )
                    })
                  }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
})

export default Tokens;

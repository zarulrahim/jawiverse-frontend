import Head from 'next/head'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
// import styles from '../styles/Home.module.css'
// import Header from "./components/Header"
import axios from 'axios';
import Web3 from 'web3';
// import '/public/pentasSmartContractAbi.json'
// import pentasContractAbi from "./public/pentasSmartContractAbi.json";
// import { useKeenSlider } from "keen-slider/react"
import Slider from 'react-slick'
import { FiShare2, FiHeart, FiSearch, FiChevronUp, FiChevronDown } from 'react-icons/fi';
import ReactPlayer from 'react-player';
import { Tab, Disclosure } from '@headlessui/react'
import { useRouter } from 'next/router';
import { observer } from 'mobx-react-lite';
import { getSnapshot } from 'mobx-state-tree';
import { mstGeneral } from '../mobx';
import moment from 'moment';

const Leaderboard = observer((props) => {
  const { query, locale, locales, defaultLocale, asPath } = useRouter();
  const router = useRouter();
  useEffect(() => {
    mstGeneral.fetchLeaderboard();
  }, [])

  useEffect(() => {
    if (router !== undefined) {
      console.log("current router ===> ", router.query)
      const params = {
        ...router.query
      }
      mstGeneral.fetchTokens(params)
    }
  }, [router])

  return (
    <div className='min-h-screen'>  
      {/* <div id="roadmap" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-slate-900 bg-opacity-20 py-10 ">
          <div>
            <div className="text-center">
              <h1 className="text-white text-2xl md:text-5xl">Roadmap</h1>
              <h5 className="mt-4 text-gray-400 text-xl">Good roadmap delivers good outcome!</h5>
            </div>
            <div className='px-4 sm:px-10 mt-10 flex items-start'>
              
            </div>
          </div>
        </div>
      </div> */}
      <div id="rumi" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-gray-900 bg-opacity-80 py-10 ">
          <div className='w-full max-w-7xl pt-20 px-4 md:px-0'>
            <div className='mt-0 flex justify-between items-center'>
              <div>
                <h1 className="text-white uppercase font-bold text-3xl md:text-4xl">Top Holders</h1>
                <h5 className="mt-2 text-gray-400 text-lg">Jawispace all-time top holders ranked by amount purchase</h5>
              </div>
              <div className='flex'>
                {/* <FiShare2 className='ml-5 text-white text-3xl' /> */}
              </div>
            </div>
            <div className='w-full mt-6 border-t border-gray-200 dark:border-gray-700'>
            <div className='flex justify-between items-center mt-4'>
              <div className='text-xl text-white'>{getSnapshot(mstGeneral).holder_leaderboard?.length || 0} Holders</div>
              <div className='text-gray-400'>Last updated on {moment(getSnapshot(mstGeneral).holder_leaderboard[0]?.updated_at).format("Do MMM YYYY hh:mm")}</div>
            </div>
            <div className='grid grid-cols-1 md:grid-cols-1 gap-0 md:gap-0 mt-0'>                
              <div className="container mx-auto max-w-full">
                  <div className="py-0">
                      <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                          <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
                            <div className="grid grid-cols-2 md:grid-cols-4">
                              <div className='md:col-span-1 px-5 py-3 bg-slate-700 border-b border-slate-700 text-white text-left text-sm uppercase font-normal'>Holder</div>
                              <div className='md:col-span-3 px-5 py-3 bg-slate-700 border-b border-slate-700 text-white text-left text-sm uppercase font-normal'>Collections</div>
                            </div>
                            {
                              getSnapshot(mstGeneral)?.holder_leaderboard?.map((item, index) => {
                                return (
                                  <div key={index} className="grid grid-cols-2 md:grid-cols-4 flex items-center bg-slate-800 text-white text-sm border-b border-slate-700">
                                    <div className='md:col-span-1'>
                                      <div className="flex items-center mb-0 px-5 py-5">
                                        <div className="mr-5">{index + 1}</div>
                                        <div className="flex-shrink-0">
                                          <a target="_blank" rel="noreferrer"  href={"https://app.pentas.io/user/" + item?.address} className="block relative">
                                            <img alt={item?.username} src={item?.profile_image_url} className="mx-auto object-cover rounded-full h-10 w-10 "/>
                                          </a>
                                        </div>
                                        <div className="ml-3">
                                          <p className="text-white whitespace-no-wrap line-clamp-1">
                                            {item?.name}
                                          </p>
                                          <p className="text-gray-400 cursor-pointer hover:text-gray-500 whitespace-no-wrap">
                                            {
                                              item?.twitter !== "" ?
                                              (
                                                <a href={"https://twitter.com/" + item?.twitter} target="_blank" rel="noreferrer">@{item?.twitter}</a>
                                              )
                                              :
                                              (
                                                <a href={"https://app.pentas.io/user/" + item?.address} target="_blank" rel="noreferrer">@{item?.username}</a>
                                              )
                                            }
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                    <div className='md:col-span-3'>
                                      <div className="px-5 py-5 bg-slate-800 text-center">
                                        <div className='grid grid-cols-3 md:grid-cols-10 gap-2'>
                                        {
                                          item?.collections?.map((collection, collectionIndex) => {
                                            return (
                                              <a key={collectionIndex} target="_blank" href={collection?.marketplace_url} rel="noreferrer" className="block relative">
                                                <img alt={collection?.id} src={collection?.poster_url} className="rounded-lg mx-auto"/>
                                              </a>
                                            )
                                          })
                                        }
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                )
                              })
                            }

                            



                            {/* <table className="min-w-full leading-normal">
                              <thead>
                                <tr>
                                  <th scope="col" className="px-5 py-3 bg-slate-700 border-b border-slate-700 text-white text-left text-sm uppercase font-normal">
                                    Holder
                                  </th>
                                  <th scope="col" className="px-5 py-3 bg-slate-700 border-b border-slate-700 text-white  text-center text-sm uppercase font-normal">
                                    Collections
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                {
                                  getSnapshot(mstGeneral)?.holder_leaderboard?.slice(0,10).map((item, index) => {
                                    return (
                                      <tr>
                                        <td className="px-5 py-5 border-b border-slate-700 bg-slate-800 text-white text-sm">
                                          <div className="flex items-center">
                                            <div className="mr-5">{index + 1}</div>
                                            <div className="flex-shrink-0">
                                              <a href="#" className="block relative">
                                                <img alt={item?.username} src={item?.profile_image_url} className="mx-auto object-cover rounded-full h-10 w-10 "/>
                                              </a>
                                            </div>
                                            <div className="ml-3">
                                              <p className="text-white whitespace-no-wrap">
                                                {item?.name}
                                              </p>
                                              <p className="text-gray-400 cursor-pointer hover:text-gray-500 whitespace-no-wrap">
                                                <a href={"https://twitter.com/" + item?.username} target="_blank">@{item?.username}</a>
                                              </p>
                                            </div>
                                          </div>
                                        </td>
                                        <td className="px-5 py-5 border-b border-slate-700 bg-slate-800 text-center">
                                            <div className='grid grid-cols-2 md:grid-cols-10 gap-2'>
                                            {
                                              item?.collections?.map((collection, collectionIndex) => {
                                                return (
                                                  <a target="_blank" href={collection?.marketplace_url} className="block relative">
                                                    <img alt={collection?.id} src={collection?.poster_url} className="rounded-lg mx-auto"/>
                                                  </a>
                                                )
                                              })
                                            }
                                            </div>
                                        </td>
                                      </tr>
                                    )
                                  })
                                }
                              </tbody>
                            </table> */}
                          {/* <div className="px-5 bg-white py-5 flex flex-col xs:flex-row items-center xs:justify-between">
                              <div className="flex items-center">
                                  <button type="button" className="w-full p-4 border text-base rounded-l-xl text-gray-600 bg-white hover:bg-gray-100">
                                      <svg width="9" fill="currentColor" height="8" className="" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M1427 301l-531 531 531 531q19 19 19 45t-19 45l-166 166q-19 19-45 19t-45-19l-742-742q-19-19-19-45t19-45l742-742q19-19 45-19t45 19l166 166q19 19 19 45t-19 45z">
                                          </path>
                                      </svg>
                                  </button>
                                  <button type="button" className="w-full px-4 py-2 border-t border-b text-base text-indigo-500 bg-white hover:bg-gray-100 ">
                                      1
                                  </button>
                                  <button type="button" className="w-full px-4 py-2 border text-base text-gray-600 bg-white hover:bg-gray-100">
                                      2
                                  </button>
                                  <button type="button" className="w-full px-4 py-2 border-t border-b text-base text-gray-600 bg-white hover:bg-gray-100">
                                      3
                                  </button>
                                  <button type="button" className="w-full px-4 py-2 border text-base text-gray-600 bg-white hover:bg-gray-100">
                                      4
                                  </button>
                                  <button type="button" className="w-full p-4 border-t border-b border-r text-base  rounded-r-xl text-gray-600 bg-white hover:bg-gray-100">
                                      <svg width="9" fill="currentColor" height="8" className="" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z">
                                          </path>
                                      </svg>
                                  </button>
                              </div>
                            </div>*/}
                        </div>
                      </div>
                  </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
})

export default Leaderboard;

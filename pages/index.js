import Head from 'next/head'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import styles from '../styles/Home.module.css'
// import Header from "./components/Header"
import axios from 'axios';
import Web3 from 'web3';
import moment from 'moment';
// import '/public/pentasSmartContractAbi.json'
// import pentasContractAbi from "./public/pentasSmartContractAbi.json";
// import { useKeenSlider } from "keen-slider/react"
import Slider from 'react-slick'
import { FiZap, FiStar, FiTag, FiGrid } from 'react-icons/fi';
import { FaDiscord, FaInstagram, FaTwitter, FaFacebook, FaPlayCircle, FaTiktok } from 'react-icons/fa';
import ReactPlayer from 'react-player';
import { useRouter } from 'next/router';
import { Tab } from '@headlessui/react'
import { observer } from 'mobx-react-lite';
import { getSnapshot } from 'mobx-state-tree';
import { mstGeneral } from '../mobx';
import dynamic from 'next/dynamic'

function Arrow(props) {
  const disabeld = props.disabled ? " arrow--disabled" : ""
  return (
    <svg
      onClick={props.onClick}
      className={`arrow ${
        props.left ? "arrow--left" : "arrow--right"
      } ${disabeld}`}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
    >
      {props.left && (
        <path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z" />
      )}
      {!props.left && (
        <path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z" />
      )}
    </svg>
  )
}

const faqs = [
  {
    title: "What is JAWISPACE ?",
    content: `Founded on 2020 in Silicon Valley of Malaysia, Cyberjaya, Jawispace is proudly introduce their first-ever Intellectual Properties (IP) called <b>Rumi</b>. <br /><br /> Jawispace is an animation startup and currently active in the animation industry. As the team members are well-experienced in the animation industry, Jawispace is committed to create and produce more IPs in the future.`
  },
  {
    title: 'Do we need to hold any tokens in order to access the contents?',
    content: `Our contents will be released publicly. There's no restriction or such a thing. However, the token holders could gain the benefits such as special discount to our merchandise, whitelist, nft giveaway, get early access to our milestone, and more.`
  },
  {
    title: 'What is the JAWISPACE NFT roadmap?',
    content: `We believe that most of the people are expecting for a clear and complete structure when it comes to the roadmap and utilities. We do have our plan lined up but as we are still engaging with new ideas and expansion plan, we gonna update and announce our roadmap and utilities based on current capabilities that we have. This is to ensure that we can achieve our milestones expectedly and not overpromised. <br /><br /> <a class="text-red-500 hover:text-red-600" target="_blank" href="https://whitepaper.jawispace.io/nft-campaign/what-is-jawix/roadmap-1.0" rel="noreferrer">Roadmap 1.0</a>`
  },
  {
    title: 'What is the JAWISPACE NFT token ecosystem?',
    content: `We will have <b>Access (JACS)</b> and <b>Asset (JAST)</b> token. Each type will have their own benefits. You can read more about it <a class="text-red-500 hover:text-red-600" href="https://whitepaper.jawispace.io/fundamental/tokens-and-utilities" target="_blank">here</a>`
  },
  {
    title: 'Where the NFT will be minted?',
    content: `Our tokens will be minted in <a class="text-red-500 hover:text-red-600" target="_blank" href="https://app.pentas.io/user/jawispace">Pentas.io</a> marketplace which will be minted forever inside the Binance Smart Chain.`
  }
];

const teams = [
  {
    name: "RoachPunks",
    twitter: "RoachPunks",
    designation: "NFT Advisor",
    words: "Determination is key!",
    imageUrl: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/teams/images/248dd56e-6091-400f-b0f4-f7e28f663b0a.jpg",
  },
  {
    name: "Hafiy",
    twitter: "thehafiy",
    designation: "Founder",
    words: "Determination is key!",
    imageUrl: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/teams/images/hafiy-pfp.jpg",
  },
  {
    name: "Freddyz",
    twitter: "freddyzeth",
    designation: "Co-Founder",
    words: "Practice makes perfect!",
    imageUrl: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/teams/images/freddyz-pfp.jpg",
  },
  {
    name: "Ezzul",
    twitter: "",
    designation: "Concept & Benchmark",
    words: "Believe in everything you do!",
    imageUrl: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/teams/images/itoi-pfp.jpg",
  },
  {
    name: "Atiqah",
    twitter: "",
    designation: "Scriptwriter",
    words: "",
    imageUrl: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/teams/images/kakika-pfp.jpg",
  },
  // {
  //   name: "Tasha",
  //   twitter: "",
  //   designation: "Social Media",
  //   words: "",
  //   imageUrl: "https://ui-avatars.com/api/?name=Tasha",
  // },
];

const nfts = [
  {
    name: "Hafiy",
    twitter: "hafi_y",
    designation: "Founder",
    words: "Determination is key!",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Kcg Bijau",
    twitter: "kcg_bijau",
    designation: "Co-Founder",
    words: "Practice makes perfect!",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Itoi",
    twitter: "ito_i",
    designation: "Artist",
    words: "Believe in everything you do!",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Amira",
    twitter: "amira_nft",
    designation: "Copywriter",
    words: "",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Amira",
    twitter: "amira_nft",
    designation: "Copywriter",
    words: "",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Amira",
    twitter: "amira_nft",
    designation: "Copywriter",
    words: "",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
];

const utilities = [
  {
    name: 'Early Access',
    description:
      'Gain an early access to our exclusive milestones in Jawispace before it dropped such as, animated series, special NFTs, tutorial, and more.',
    icon: FiZap,
  },
  {
    name: 'Exclusive Merchandise',
    description:
      'Enjoy our exclusive merchandises as a reward of being our holders. Besides that, holders also can enjoy interesting discount for every merchandises that we sell.',
    icon: FiTag,
  },
  {
    name: 'First-ever 3D Animated Series Campaign',
    description:
      'Our main project, <b>RUMI</b> is the first-ever 3D animated series campaign that exists in the Pentas.io.',
    icon: FiStar,
  },
  {
    name: 'Interesting Web Features',
    description:
      'We will utilise our website to become as one-stop centre for our holders. Holders can just connect their wallet in our website and enjoy variety of features including whitelist redemption, voting and more.',
    icon: FiGrid,
  },
  {
    name: 'Transfers are instant',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.',
    icon: FiZap,
  },
  {
    name: 'Mobile notifications',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.',
    icon: FiZap,
  },
]

const contentWithTranslation = {
  'en': {
    'welcomeTitle': "Welcome to",
    'tagline': 'The ultimate space for the endless adventure!',
  },
  'my': {
    'welcomeTitle': "Selamat Datang ke",
    'tagline': 'Dunia Pengembaraan  yang tiada penghujungnya!',
  },
}

const Home = observer((props) => {
  const { locale, locales, defaultLocale, asPath } = useRouter();
  const content = contentWithTranslation[locale]
  // const [videoUrl, setVideoUrl] = useState("")
  const [title, setTitle] = useState("jawiverse.io")
  const [currentSlide, setCurrentSlide] = useState(0)
  const [loaded, setLoaded] = useState(false)
  // const [sliderRef, instanceRef] = useKeenSlider({
  //   initial: 0,
  //   slideChanged(slider) {
  //     setCurrentSlide(slider.track.details.rel)
  //   },
  //   created() {
  //     setLoaded(true)
  //   },
  //   breakpoints: {
  //     "(min-width: 400px)": {
  //       slides: { perView: 2, spacing: 5 },
  //     },
  //     "(min-width: 1000px)": {
  //       slides: { perView: 5, spacing: 20 },
  //     },
  //   },
  //   slides: { perView: 1 },
  // })

  // const convertedVideoSrc = async (stringUrl) => {
  //   // var URL = window.URL || window.webkitURL;
  //   let file = await fetch(stringUrl).then(r => r.blob());
  //   let url = URL.createObjectURL(file);
  //   return url;
  //   // return blob[0];
  //   // axios.get(stringUrl, {
  //   //   responseType: 'arraybuffer',
  //   //   headers: {
  //   //     'Content-Type': 'application/json'
  //   //   }
  //   // })
  //   // .then((response) => {
  //   //   console.log("check response ===> ", response)
  //   // })
  //   // console.log("check stringUrl ===> ", stringUrl)
  // }

  useEffect(() => {
    mstGeneral.fetchSingleShow();
    mstGeneral.fetchTokens();
    mstGeneral.fetchLeaderboard();
  }, [])

  // useEffect(() => {
    // script for url to blob conversion
    // fetchVideo('https://media.vimejs.com/720p.mp4').then(function(blob) {
    //   var blobUrl = URL.createObjectURL(blob);
    //   setVideoUrl(blobUrl)
    //   console.log(`Changing video source to blob URL "${blobUrl}"`)
    // });
  // }, [])

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  };

  const characterSliderSettings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          // infinite: true,
          // dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          // arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  }

  const characters = [
    {
      id: 1,
      name: "Rumi",
      type: 'Protagonist',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/rumcomp_00000.png"
    },
    {
      id: 2,
      name: "Potron",
      type: 'Protagonist',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/potroncomp_00000.png"
    },
    {
      id: 3,
      name: "Ling",
      type: 'Antagonist',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/lingcomp_00000.png"
    },
    {
      id: 4,
      name: "Raz",
      type: 'Villain',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/raz-comp.png"
    }
  ]

  // const onClickFaqs = id => {
  //   const array = selectedFaqs.slice()
  //   const found = array?.find(faq => faq === id)
  //   if (found) {
  //     setSelectedFaqs(current => current?.filter(faq => faq !== id))
  //   } else {
  //     setSelectedFaqs(current => current?.push(id))
  //   }
  //   console.log("check id ==> ", id, "check selectedFaqs ==> ", selectedFaqs)
  // }
  // let selectedAccount;
  // let pentasContract;
  // let currentBalance;
  // let isInitialized = false;
  // const init = async () => {
  //   let provider = window.ethereum;
  //   if (typeof provider !== 'undefined') {
  //     provider
  //       .request({ method: 'eth_requestAccounts' })
  //       .then((accounts) => {
  //         selectedAccount = accounts[0];
  //         console.log(`Selected account is ${selectedAccount}`);
  //       })
  //       .catch((err) => {
  //         console.log(err);
  //         return;
  //       });

  //     window.ethereum.on('accountsChanged', function (accounts) {
  //       selectedAccount = accounts[0];
  //       console.log(`Selected account changed to ${selectedAccount}`);
  //     });
  //   }

  //   const web3 = new Web3(provider);
  //   const networkId = await web3.eth.net.getId();

  //   const src = "/pentasSmartContractAbi.json"
  //   const response = await axios.get(src)
  //   console.log("check abi contract ===> ", JSON.parse(response.data.result))
  //   const pentasContractAbi = JSON.parse(response.data.result)

  //   pentasContract = new web3.eth.Contract(
  //     pentasContractAbi,
  //     "0x3aFa102B264b5f79ce80FED29E0724F922Ba57c7"
  //   );
  //   // const tokenIds = [185752, 185742];
  //   // tokenIds.map((id) => {
  //   //   pentasContract.methods.
  //   // })
  //   // pentasContract.methods.ownerOf(185752)
  //   // .call()
  //   // .then((address) => {
  //   //   console.log("Check owner of the token 185752 ===> ", address)
  //   // })
  //   pentasContract.methods.balanceOf(selectedAccount)
  //   .call()
  //   .then((balance) => {
  //     console.log("Check balance of the address ===> ", selectedAccount, balance)
  //     for (var i = 0; i < balance; i++) {
  //       pentasContract.methods.tokenOfOwnerByIndex(selectedAccount, i)
  //       .call()
  //       .then((id) => {
  //         console.log("check token id ===> ", i, id)
  //         pentasContract.methods.tokenURI(id)
  //         .call()
  //         .then((token) => {
  //           console.log("check token uri ===> ", i, token)
  //         })
  //       })
  //     }
  //   })
  //   isInitialized = true;
  // }

  useEffect(() => {
    // init()
  }, [])

  const VideoPlayer = dynamic(() => import('./plugins/player'), {
    ssr: false,
  })

  const FaqItem = (props) => {
    const [expandedFaq, setExpandedFaq] = useState(false)

    return (
      <div className='px-4 mb-4'>
        <a className="cursor-pointer" onClick={() => { setExpandedFaq(!expandedFaq) }}>
          <div className='w-full rounded-lg bg-red-600 flex justify-between p-4'>
            <div className='text-white'>{props.item.title}</div>
          </div>
        </a>
        <div className={`${expandedFaq ? `` : `hidden` } mt-4 mb-8 px-4 text-white`}>
          <div dangerouslySetInnerHTML={{__html: props.item.content }} />
        </div>
      </div>
    )
  };

  const TeamItem = (props) => {
    return (
      <div className="text-center">
        <div className="flex">
          <img className="w-32 h-32 mx-auto rounded-full border border-4 border-white" src={props.item.imageUrl} alt={props.item.name} />
        </div>
        <div className="text-sm mt-4">
        <a className="text-red-500 hover:text-red-600" href={`https://twitter.com/${props.item.twitter}`} target="_blank" rel="noreferrer"><p className="text-2xl text-white leading-none">{props.item.name}</p></a>
          <p className="text-gray-400 mt-2">{props.item.designation}</p>
          {
            false ?
            (
              <p className="text-white mt-1"><a className="text-red-500 hover:text-red-600" href={`https://twitter.com/${props.item.twitter}`} target="_blank" rel="noreferrer">@{props.item.twitter}</a></p>
            )
            :
            (
              null
            )
          }
          {/* <p className="text-gray-400 mt-1">{props.item.words}</p> */}
        </div>
      </div>
    )
  };

  return (
    <div className='min-h-screen'>  
      <div className="-z-50 md:h-screen h-halfScreen bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-main-rumi-jawispace-3000.jpg")' }}>
        <div className="flex items-center justify-center h-full w-full bg-slate-900 bg-opacity-80">
          <div className="px-4 mt-10 md:mt-0 md:px-0 text-center">
              <h1 className="text-white text-3xl md:text-6xl">{content.welcomeTitle} <span className="font-bold text-white">JAWISPACE</span></h1>
              <h5 className="mt-0 md:mt-5 text-gray-400 text-lg md:text-2xl">{content.tagline}</h5>
              <div className='relative top-10'>
                <div className='md:flex md:justify-center'>
                  <div className='mr-2 ml-2'>
                    <a href="#rumi"><div className="w-full text-white bg-red-600 border border-red-600 hover:bg-red-700 font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700">{ locale === 'en' ? "Explore Rumi NFT Project" : "Explore Projek NFT Rumi" }</div></a>
                  </div>
                  <div className='mr-2 ml-2'>
                    <a href="#community"><div className="w-full mt-2 md:mt-0 text-white bg-transparent md:border border-white dark:border-white hover:border-white hover:bg-white font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-transparent dark:hover:bg-white hover:text-black cursor-pointer">{ locale === 'en' ? "Join Community" : "Sertai Komuniti" }</div></a>
                    {/* <a href="https://whitepaper.jawispace.io" target="_blank" rel='noreferrer' type="button" className="w-full mt-2 md:mt-0 text-white bg-transparent md:border border-white dark:border-white hover:border-red-600 hover:bg-red-600 font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-transparent dark:hover:bg-red-600">{ locale === 'en' ? "Read Whitepaper" : "Baca Whitepaper" }</a> */}
                  </div>
                  {/* <div className='mr-2 ml-2'>
                    <button type="button" className="text-white bg-red-700 hover:bg-red-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-3 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700">Our Utilities</button>
                  </div>
                  <div className='mr-2 ml-2'>
                    <button type="button" className="text-white bg-red-700 hover:bg-red-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-3 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700">NFTs</button>
                  </div>
                  <div className='mr-2 ml-2'>
                    <button type="button" className="text-white bg-red-700 hover:bg-red-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-3 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700">FAQs</button>
                  </div> */}
                </div>
              </div>
          </div>
        </div>
      </div>
      {/* <div id="roadmap" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-slate-900 bg-opacity-20 py-10 ">
          <div>
            <div className="text-center">
              <h1 className="text-white text-2xl md:text-5xl">Roadmap</h1>
              <h5 className="mt-4 text-gray-400 text-xl">Good roadmap delivers good outcome!</h5>
            </div>
            <div className='px-4 sm:px-10 mt-10 flex items-start'>
              
            </div>
          </div>
        </div>
      </div> */}
      <div id="rumi" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-gray-900 bg-opacity-50 py-10 px-4 md:px-0">
          <div className='w-full max-w-7xl'>
            <div className="text-center">
              <h1 className="text-white text-3xl md:text-5xl">The Adventure of <span className="font-bold text-red-600">{getSnapshot(mstGeneral)?.showData?.title}</span>  </h1>
              <h5 className="mt-2 md:mt-5 text-gray-400 text-lg md:text-2xl">The First Southeast Asian 3D Animated Series To Use NFT!</h5>
            </div>
            <div className='w-full px-4 sm:px-10 mt-6 md:mt-10'>
              <div className="grid grid-cols-1 md:grid-cols-3 gap-5" id="first" role="tabpanel" aria-labelledby="first-tab">
                <div className='w-full col-span-2 mb-4 md:mb-0 md:mr-10'>
                  <div className='rounded-lg overflow-hidden'>
                    <VideoPlayer
                      type="simpleUi"
                      poster="https://jawiverse-bucket.sgp1.digitaloceanspaces.com/episodes/19/images/jw-io-episode-img-19-1645715884EmBr4u.png"
                      videoSrc="https://jawiverse-bucket.sgp1.digitaloceanspaces.com/episodes/19/videos/jw-io-episode-video-19-16457158860WTvVU.m3u8"
                    />
                  </div>
                  <Tab.Group>
                    <Tab.List className="hover:ring-0 hover:ring-offset-0 mb-4 mt-4 flex justify-start border-b border-gray-200 dark:border-gray-700">
                      <Tab as={React.Fragment}>
                      {({ selected }) => (
                        <button
                          className={
                            `inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                          }
                        >
                          Description
                        </button>
                      )}
                      </Tab>
                      <Tab as={React.Fragment}>
                      {({ selected }) => (
                        <button
                          className={
                            `inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                          }
                        >
                          Characters
                        </button>
                      )}
                      </Tab>
                      <Tab as={React.Fragment}>
                      {({ selected }) => (
                        <button
                          className={
                            `inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                          }
                        >
                          Credits
                        </button>
                      )}
                      </Tab>
                    </Tab.List>
                    <Tab.Panels>
                      <Tab.Panel>
                        <div className='mt-5'>
                          <div className='text-lg text-white border-l-8 rounded-md bg-gray-800 border-red-600 px-4 py-2 mb-5'>Synopsis</div>
                          <div className='text-md text-white'>{getSnapshot(mstGeneral)?.showData?.summary}</div>
                        </div>
                        <div className='mt-5'>
                          <div className='text-lg text-white border-l-8 rounded-md bg-gray-800 border-red-600 px-4 py-2 mb-5'>Project Details</div>
                          <table className="table-auto">
                            <tbody>
                              <tr>
                                <td><div className='text-md text-white'>Title</div></td>
                                <td><div className='text-md text-white ml-10'>The Adventure of {getSnapshot(mstGeneral)?.showData?.title}</div></td>
                              </tr>
                              <tr>
                                <td><div className='text-md text-white'>Genre</div></td>
                                <td><div className='text-md text-white ml-10'>{getSnapshot(mstGeneral)?.showData?.genres?.map(g => g).join(', ')}</div></td>
                              </tr>
                              <tr>
                                <td><div className='text-md text-white'>Audio</div></td>
                                <td><div className='text-md text-white ml-10'>Bahasa Malaysia</div></td>
                              </tr>
                              <tr>
                                <td><div className='text-md text-white'>Subtitles</div></td>
                                <td><div className='text-md text-white ml-10'>English, Mandarin</div></td>
                              </tr>
                              <tr>
                                <td><div className='text-md text-white'>Episodes</div></td>
                                <td><div className='text-md text-white ml-10'>{getSnapshot(mstGeneral)?.showData?.seasons?.length > 0 ? getSnapshot(mstGeneral)?.showData?.seasons[0]?.episodes?.length : 0} Episodes (4 minutes per episode)</div></td>
                              </tr>
                            </tbody>
                          </table>
                        </div> 
                      </Tab.Panel>
                      <Tab.Panel>
                        <div className="w-full px-4" id="second" role="tabpanel" aria-labelledby="second-tab">
                          <Slider {...characterSliderSettings} className="w-full">
                          {
                            characters.map((item, index) => {
                              return (
                                <div key={index} className={`${index !== characters.length - 1 ? 'p-3' : 'p-3'}`}>
                                  <div className={`max-w-full rounded-lg overflow-hidden`}>
                                    <div className='flex justify-center'>
                                      <img className="w-56" src={item?.image_url} alt={item?.name} />
                                    </div>
                                    <div className="text-center px-4 py-2 mt-2">
                                      <div className=" text-white text-xl mb-0">{item?.name}</div>
                                      <div className=" text-gray-500 text-md mb-0">{item?.type}</div>
                                      {/* <p className="text-gray-700 text-base">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                                        </p>*/}
                                    </div>
                                  </div>
                                </div>
                              )
                            })
                          }
                          </Slider>
                        </div>
                      </Tab.Panel>
                      <Tab.Panel>
                        <div className="w-full" id="third" role="tabpanel" aria-labelledby="third-tab">
                          <div className='max-w-3xl w-full flex mx-auto items-start mt-5 grid grid-cols-1 md:grid-cols-3'>
                            <div>
                              <div className='text-center'>
                                <div className='text-md text-gray-400'>Directed by</div>
                                <div>
                                  <div className='text-md text-white'>Hafiy</div>
                                </div>
                              </div>
                            </div>
                            <div className='mt-2 md:mt-0'>
                              <div className='text-center'>
                                <div className='text-md text-gray-400'>Concept & Benchmark</div>
                                <div>
                                  <div className='text-md text-white'>Ezzul</div>
                                </div>
                              </div>
                            </div>
                            <div className='mt-2 md:mt-0'>
                              <div className='text-center'>
                                <div className='text-md text-gray-400'>Scriptwriter</div>
                                <div>
                                  <div className='text-md text-white'>Atiqah</div>
                                </div>
                              </div>
                            
                            </div>
                          </div>
                          <div className='max-w-3xl w-full flex mx-auto items-start mt-2 md:mt-10 grid grid-cols-1 md:grid-cols-3'>
                            <div className='text-center'>
                              <div className='text-md text-gray-400'>Cast</div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Amirul Arif <span className="text-gray-400">as</span> Rumi</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Yasmeen <span className="text-gray-400">as</span> Ling</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Amirul Arif <span className="text-gray-400">as</span> Potron</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>TBA <span className="text-gray-400">as</span> Raz</div>
                              </div>
                            </div>
                            <div className='text-center mt-2 md:mt-0'>
                              <div className='text-md text-gray-400'>Crew</div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Adam <span className="text-gray-400">as</span> Storyboard</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Fahmie <span className="text-gray-400">as</span> Compositor</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Hafizul <span className="text-gray-400">as</span> Compositor</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Haizeel <span className="text-gray-400">as</span> Sketch FX</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Hakim <span className="text-gray-400">as</span> Animator</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Syafiq <span className="text-gray-400">as</span> Animator</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Azeril <span className="text-gray-400">as</span> Modeller</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Nazib <span className="text-gray-400">as</span> Rigger</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Rais <span className="text-gray-400">as</span> Modeller</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Nazmeen <span className="text-gray-400">as</span> Audio Designer</div>
                              </div>
                              <div className='flex justify-center'>
                                <div className='text-md text-white'>Afiq <span className="text-gray-400">as</span> Music Composer</div>
                              </div>
                            </div>
                            <div className='text-center mt-4 md:mt-0'>
                              <div className='text-md text-gray-400'>Supported By</div>
                              <div className='flex justify-center mx-auto mt-0 md:mt-2'>
                                <img src="http://mbitsdigital.com/wp-content/uploads/2021/09/mdec-logo.png" width={'50%'} />
                              </div>
                            </div>
                          </div>
                        </div>
                      </Tab.Panel>
                    </Tab.Panels>
                  </Tab.Group>
                </div>
                <div className='w-full'>
                  <img className='rounded-xl shadow-xl overflow-hidden' width="100%" src="https://jawiverse-bucket.sgp1.digitaloceanspaces.com/poster-rumi-v2.jpg" />
                  <div className='flex mt-4 md:block'>
                    <div className='w-full mr-4 md:mb-4'>
                      <Link href={`watch/${getSnapshot(mstGeneral)?.showData?.slug}`} locale={locale}><div className="w-full text-white bg-red-600 border border-red-600 hover:bg-red-700 font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700 cursor-pointer">Watch Now</div></Link>
                    </div>
                    <div className='w-full'>
                      <a href="https://app.pentas.io/user/0x87b463374Ee072DaDD19D5Eb67878c93e9757352" target="_blank" rel="noreferrer"><div className="w-full text-white bg-transparent border border-white hover:bg-red-700 font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-transparent hover:text-black dark:hover:bg-white cursor-pointer">Purchase NFT</div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="nfts" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-slate-900 bg-opacity-20 py-10 ">
          <div className='w-full'>
            <div className="px-4 md:px-0 text-center">
              <h1 className="text-white text-3xl md:text-5xl">NFTs</h1>
              <h5 className="mt-2 md:mt-5 text-gray-400 text-lg md:text-2xl">Our minted token collections</h5>
            </div>
            <div className="max-w-7xl mx-auto px-4 sm:px-10 mt-6 md:mt-10">
              <Slider {...settings} className="w-full">
                {
                  getSnapshot(mstGeneral)?.tokens?.map((item, index) => {
                    return (
                      <div key={index} className={`focus:ring-0 p-3`}>
                        <div className={`rounded-lg overflow-hidden max-w-full shadow-lg bg-white`}>
                          <img className="w-full" src={item?.poster_url} alt="" />
                          <div className="px-4 py-2">
                            <div className="text-md mb-0 line-clamp-1">{item?.title}</div>
                            {/* <p className="text-gray-700 text-base">
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                              </p>*/}
                          </div>
                          <div className="px-4 pt-0 pb-2">
                            <span className="inline-block bg-gray-200 rounded-lg px-2 py-1 text-xs text-gray-700 mr-2 mb-1">{item?.token_type}</span>
                          </div>
                        </div>
                      </div>
                    )
                  })
                }
              </Slider>
              <div className='w-full flex justify-center mt-2 md:mt-6'>
                <Link href="/tokens">
                  <div className="text-white bg-red-600 border border-red-600 hover:bg-red-700 font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700 cursor-pointer">Browse all NFTs</div>
                </Link>
              </div>
            </div>
            {/* <div className="navigation-wrapper max-w-7xl px-4 sm:px-10 mt-10 flex items-start">
              <div ref={sliderRef} className="keen-slider">
              {
                nfts.map((item, index) => {
                  return (
                    <div className="max-w-full rounded-lg overflow-hidden shadow-lg bg-white">
                      <img className="w-full" src="https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg" alt="" />
                      <div className="px-4 py-2">
                        <div className="font-bold text-md mb-0">The Coldest Sunset</div>
                      </div>
                      <div className="px-4 pt-0 pb-2">
                        <span className="inline-block bg-gray-200 rounded-full px-2 py-1 text-sm font-semibold text-gray-700 mr-2 mb-1">Access Pass</span>
                      </div>
                    </div>
                  )
                })
              }
              </div>
              {loaded && instanceRef.current && (
                <>
                  <Arrow
                    left
                    onClick={(e) =>
                      e.stopPropagation() || instanceRef.current?.prev()
                    }
                    disabled={currentSlide === 0}
                  />

                  <Arrow
                    onClick={(e) =>
                      e.stopPropagation() || instanceRef.current?.next()
                    }
                    disabled={
                      currentSlide ===
                      instanceRef.current.track.details.slides.length - 1
                    }
                  />
                </>
              )}
            </div>
            {loaded && instanceRef.current && (
              <div className="dots mt-4">
                {[
                  ...Array(instanceRef.current.track.details.slides.length).keys(),
                ].map((idx) => {
                  return (
                    <button
                      key={idx}
                      onClick={() => {
                        instanceRef.current?.moveToIdx(idx)
                      }}
                      className={"dot" + (currentSlide === idx ? " active" : "")}
                    ></button>
                  )
                })}
              </div>
              )} */}
          </div>
        </div>
      </div>
      <div id="leaderboard" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="px-4 md:px-0 h-full w-full bg-gray-900 bg-opacity-50 py-10 ">
          <div className='max-w-7xl mx-auto px-4 sm:px-10'>
            <div className="text-center">
              <h1 className="text-white text-3xl md:text-5xl">Top 10 Holders</h1>
              <h5 className="max-w-2xl mx-auto  mt-2 md:mt-5 text-gray-400 text-lg md:text-2xl">Jawispace all-time top holders</h5>
            </div>
            <div className='flex justify-between items-center mt-4'>
              <div className='text-xl text-white'>{getSnapshot(mstGeneral).holder_leaderboard?.length || 0} Holders</div>
              <div className='text-gray-400'>Last updated on {moment(getSnapshot(mstGeneral).holder_leaderboard[0]?.updated_at).format("Do MMM YYYY hh:mm")}</div>
            </div>
            <div className='grid grid-cols-1 md:grid-cols-1 gap-0 md:gap-0 mt-0'>                
              <div className="container mx-auto max-w-full">
                  <div className="py-0">
                      <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                          <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
                            <div className="grid grid-cols-2 md:grid-cols-4">
                              <div className='md:col-span-1 px-5 py-3 bg-slate-700 border-b border-slate-700 text-white text-left text-sm uppercase font-normal'>Holder</div>
                              <div className='md:col-span-3 px-5 py-3 bg-slate-700 border-b border-slate-700 text-white text-left text-sm uppercase font-normal'>Collections</div>
                            </div>
                            {
                              getSnapshot(mstGeneral)?.holder_leaderboard?.slice(0,10).map((item, index) => {
                                return (
                                  <div key={index} className="grid grid-cols-2 md:grid-cols-4 flex items-center bg-slate-800 text-white text-sm border-b border-slate-700">
                                    <div className='md:col-span-1'>
                                      <div className="flex items-center mb-0 px-5 py-5">
                                        <div className="mr-5">{index + 1}</div>
                                        <div className="flex-shrink-0">
                                          <a target="_blank" rel="noreferrer" href={"https://app.pentas.io/user/" + item?.address} className="block relative">
                                            <img alt={item?.username} src={item?.profile_image_url} className="mx-auto object-cover rounded-full h-10 w-10 "/>
                                          </a>
                                        </div>
                                        <div className="ml-3">
                                          <p className="text-white whitespace-no-wrap line-clamp-1">
                                            {item?.name}
                                          </p>
                                          <p className="text-gray-400 cursor-pointer hover:text-gray-500 whitespace-no-wrap">
                                            {
                                              item?.twitter !== "" ?
                                              (
                                                <a href={"https://twitter.com/" + item?.twitter} target="_blank" rel="noreferrer">@{item?.twitter}</a>
                                              )
                                              :
                                              (
                                                <a href={"https://app.pentas.io/user/" + item?.address} target="_blank" rel="noreferrer">@{item?.username}</a>
                                              )
                                            }
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                    <div className='md:col-span-3'>
                                      <div className="px-5 py-5 bg-slate-800 text-center">
                                        <div className='grid grid-cols-3 md:grid-cols-10 gap-2'>
                                        {
                                          item?.collections?.map((collection, collectionIndex) => {
                                            return (
                                              <a target="_blank" href={collection?.marketplace_url} rel="noreferrer" key={collectionIndex} className="block relative">
                                                <img alt={collection?.id} src={collection?.poster_url} className="rounded-lg mx-auto"/>
                                              </a>
                                            )
                                          })
                                        }
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                )
                              })
                            }

                            



                            {/* <table className="min-w-full leading-normal">
                              <thead>
                                <tr>
                                  <th scope="col" className="px-5 py-3 bg-slate-700 border-b border-slate-700 text-white text-left text-sm uppercase font-normal">
                                    Holder
                                  </th>
                                  <th scope="col" className="px-5 py-3 bg-slate-700 border-b border-slate-700 text-white  text-center text-sm uppercase font-normal">
                                    Collections
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                {
                                  getSnapshot(mstGeneral)?.holder_leaderboard?.slice(0,10).map((item, index) => {
                                    return (
                                      <tr>
                                        <td className="px-5 py-5 border-b border-slate-700 bg-slate-800 text-white text-sm">
                                          <div className="flex items-center">
                                            <div className="mr-5">{index + 1}</div>
                                            <div className="flex-shrink-0">
                                              <a href="#" className="block relative">
                                                <img alt={item?.username} src={item?.profile_image_url} className="mx-auto object-cover rounded-full h-10 w-10 "/>
                                              </a>
                                            </div>
                                            <div className="ml-3">
                                              <p className="text-white whitespace-no-wrap">
                                                {item?.name}
                                              </p>
                                              <p className="text-gray-400 cursor-pointer hover:text-gray-500 whitespace-no-wrap">
                                                <a href={"https://twitter.com/" + item?.username} target="_blank">@{item?.username}</a>
                                              </p>
                                            </div>
                                          </div>
                                        </td>
                                        <td className="px-5 py-5 border-b border-slate-700 bg-slate-800 text-center">
                                            <div className='grid grid-cols-2 md:grid-cols-10 gap-2'>
                                            {
                                              item?.collections?.map((collection, collectionIndex) => {
                                                return (
                                                  <a target="_blank" href={collection?.marketplace_url} className="block relative">
                                                    <img alt={collection?.id} src={collection?.poster_url} className="rounded-lg mx-auto"/>
                                                  </a>
                                                )
                                              })
                                            }
                                            </div>
                                        </td>
                                      </tr>
                                    )
                                  })
                                }
                              </tbody>
                            </table> */}
                          {/* <div className="px-5 bg-white py-5 flex flex-col xs:flex-row items-center xs:justify-between">
                              <div className="flex items-center">
                                  <button type="button" className="w-full p-4 border text-base rounded-l-xl text-gray-600 bg-white hover:bg-gray-100">
                                      <svg width="9" fill="currentColor" height="8" className="" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M1427 301l-531 531 531 531q19 19 19 45t-19 45l-166 166q-19 19-45 19t-45-19l-742-742q-19-19-19-45t19-45l742-742q19-19 45-19t45 19l166 166q19 19 19 45t-19 45z">
                                          </path>
                                      </svg>
                                  </button>
                                  <button type="button" className="w-full px-4 py-2 border-t border-b text-base text-indigo-500 bg-white hover:bg-gray-100 ">
                                      1
                                  </button>
                                  <button type="button" className="w-full px-4 py-2 border text-base text-gray-600 bg-white hover:bg-gray-100">
                                      2
                                  </button>
                                  <button type="button" className="w-full px-4 py-2 border-t border-b text-base text-gray-600 bg-white hover:bg-gray-100">
                                      3
                                  </button>
                                  <button type="button" className="w-full px-4 py-2 border text-base text-gray-600 bg-white hover:bg-gray-100">
                                      4
                                  </button>
                                  <button type="button" className="w-full p-4 border-t border-b border-r text-base  rounded-r-xl text-gray-600 bg-white hover:bg-gray-100">
                                      <svg width="9" fill="currentColor" height="8" className="" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                          <path d="M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z">
                                          </path>
                                      </svg>
                                  </button>
                              </div>
                            </div>*/}
                        </div>
                      </div>
                  </div>
              </div>
            </div>
            <div className='w-full flex justify-center mt-2 md:mt-6'>
              <Link href="/leaderboard">
                <div className="text-white bg-red-600 border border-red-600 hover:bg-red-700 font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700 cursor-pointer">Show More</div>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div id="benefits" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="px-4 md:px-0 flex items-start justify-center h-full w-full bg-red-900 bg-opacity-20 py-10 ">
          <div>
            <div className="text-center">
              <h1 className="text-white text-3xl md:text-5xl">Benefits</h1>
              <h5 className="mt-2 md:mt-5 text-gray-400 text-lg md:text-2xl">The benefits in owning our NFTs</h5>
            </div>
            <div className="max-w-7xl px-4 sm:px-10 mt-10">
              <dl className="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10">
                {utilities.slice(0,4).map((utility, index) => (
                  <div key={index} className="relative">
                    <dt>
                      <div className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-red-600 text-white">
                        <utility.icon className="h-6 w-6" aria-hidden="true" />
                      </div>
                      <p className="ml-16 text-lg leading-6 font-medium text-white">{utility.name}</p>
                    </dt>
                    <div className="mt-2 ml-16 text-base text-gray-300" dangerouslySetInnerHTML={{__html: utility.description }} />
                  </div>
                ))}
              </dl>
            </div>
            <div className='w-full flex justify-center mt-10'>
              <a href="https://whitepaper.jawispace.io/fundamental/tokens-and-utilities" target="_blank" rel='noreferrer'><div className="text-white bg-red-600 border border-red-600 hover:bg-red-700 font-medium rounded-lg text-md px-5 py-3 text-center mr-0 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700">Read More</div></a>
            </div>
          </div>
        </div>
      </div>
      <div id="teams" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="px-4 md:px-0 h-full w-full bg-gray-900 bg-opacity-50 py-10 ">
          <div className='w-full'>
            <div className="text-center">
              <h1 className="text-white text-3xl md:text-5xl">Teams</h1>
              <h5 className="max-w-2xl mx-auto  mt-2 md:mt-5 text-gray-400 text-lg md:text-2xl">We believe that with the combination of our skillset, we can build something fun!</h5>
            </div>
            <div className='mt-10 grid grid-cols-2 md:grid-cols-5 gap-10 md:gap-0 max-w-7xl w-full mx-auto justify-center items-start'>
              {
                teams?.map((item, index) => {
                  return (
                    <TeamItem item={item} key={index} />
                  )
                })
              }
            </div>
          </div>
        </div>
      </div>
      <div id="community" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="px-4 md:px-0 flex items-start justify-center h-full w-full bg-red-900 bg-opacity-20 py-10 ">
          <div>
            <div className="text-center">
              <h1 className="text-white text-3xl md:text-5xl">Community</h1>
              <h5 className="mt-2 md:mt-5 text-gray-400 text-lg md:text-2xl">Let's get in touch with Jawispace community</h5>
            </div>
            <div className="max-w-7xl px-4 sm:px-10 mt-10">
              <div className='grid grid-cols-2 md:grid-cols-6 gap-5 w-full flex justify-center mt-10'>
                <a /* onClick={() => { alert("Our discord channel currently in development. We will let you know once it's ready. Thank you!") }} */ href="https://discord.com/invite/EKs5DqbamS" target="_blank" rel='noreferrer'><div className="text-white border-0 border-slate-600 font-medium rounded-lg text-md px-5 py-3 text-center mr-0 md:mr-0 bg-slate-600 hover:bg-slate-700 flex items-center cursor-pointer"><FaDiscord className='text-lg mr-4' /> Discord</div></a>
                <a href="https://twitter.com/jawispace" target="_blank" rel='noreferrer'><div className="text-white border-0 border-sky-600 font-medium rounded-lg text-md px-5 py-3 text-center mr-0 md:mr-0 bg-sky-600 hover:bg-sky-700 flex items-center"><FaTwitter className='text-lg mr-4' /> Twitter</div></a>
                <a href="https://instagram.com/jawispace" target="_blank" rel='noreferrer'><div className="text-white border-0 border-pink-600 font-medium rounded-lg text-md px-5 py-3 text-center mr-0 md:mr-0 bg-pink-600 hover:bg-pink-700 flex items-center"><FaInstagram className='text-lg mr-4' /> Instagram</div></a>
                <a href="https://facebook.com/Jawispace.NFT" target="_blank" rel='noreferrer'><div className="text-white border-0 border-blue-600 font-medium rounded-lg text-md px-5 py-3 text-center mr-0 md:mr-0 bg-blue-600 hover:bg-blue-700 flex items-center"><FaFacebook className='text-lg mr-4' /> Facebook</div></a>
                <a href="https://www.youtube.com/channel/UCP0MeFJzoSFBbkDFOTeT0jQ" target="_blank" rel='noreferrer'><div className="text-white border-0 border-red-600 font-medium rounded-lg text-md px-5 py-3 text-center mr-0 md:mr-0 bg-red-600 hover:bg-red-700 flex items-center"><FaPlayCircle className='text-lg mr-4' /> YouTube</div></a>
                <a href="https://www.tiktok.com/@jawispace" target="_blank" rel='noreferrer'><div className="text-white border-0 border-black font-medium rounded-lg text-md px-5 py-3 text-center mr-0 md:mr-0 bg-black hover:border-slate-900 hover:bg-slate-900 flex items-center"><FaTiktok className='text-lg mr-4' />TikTok</div></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="faqs" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-pattern-jawispace-1080.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-gray-900 bg-opacity-20 py-10 ">
          <div>
            <div className="text-center">
              <h1 className="text-white text-3xl md:text-5xl">FAQs</h1>
              <h5 className="mt-2 md:mt-5 text-gray-400 text-lg md:text-2xl">For those who wondering</h5>
            </div>
            <div className='w-screen max-w-7xl px-4 sm:px-10 mt-10 items-start'>
              {
                faqs?.map((item, index) => {
                  return (
                    <FaqItem item={item} key={index} />
                  )
                })
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
})

export default Home;

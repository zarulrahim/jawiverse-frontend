import React, { useEffect } from 'react'
import Head from "next/head";
import '../styles/globals.css'
import Header from './components/header'
import Footer from './components/footer'
import NextNProgress from 'nextjs-progressbar';
import { useRouter } from 'next/router'

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  // const injectScript = () => {
  //    const vimeJs = document.createElement("script");
  //    vimeJs.src = "https://cdn.jsdelivr.net/npm/@vime/core@^5/dist/vime/vime.esm.js";
  //    vimeJs.async = true;
  //    vimeJs.type = "module";
  //    document.body.appendChild(vimeJs);
  // };
  useEffect(() => {
    // injectScript()
    // router.events.on('routeChangeComplete', injectScript)
    // return () => {
    //   router.events.off('routeChangeComplete', injectScript)
    // }
  }, [])

  return (
    <React.Fragment>
      <Head>
      <title>Jawispace | Official Website</title>
      <meta name="description" content="The ultimate space for the endless adventure. Explore now!" />

      <meta itemProp="name" content="Jawispace | Official Website" />
      <meta itemProp="description" content="The ultimate space for the endless adventure. Explore now!" />
      <meta itemProp="image" content="https://jawiverse-bucket.sgp1.digitaloceanspaces.com/jawispace-wallpaper-landscape.jpg" />

      <meta property="og:url" content="https://jawispace.io" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Jawispace | Official Website" />
      <meta property="og:description" content="The ultimate space for the endless adventure. Explore now!" />
      <meta property="og:image" content="https://jawiverse-bucket.sgp1.digitaloceanspaces.com/jawispace-wallpaper-landscape.jpg" />

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content="Jawispace | Official Website" />
      <meta name="twitter:description" content="The ultimate space for the endless adventure. Explore now!" />
      <meta name="twitter:image" content="https://jawiverse-bucket.sgp1.digitaloceanspaces.com/jawispace-wallpaper-landscape.jpg" />
        <meta name="theme-color" content="#3c1742" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://unpkg.com/flowbite@latest/dist/flowbite.min.css" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit" />
        {/* <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/@vime/core@^5/themes/default.css"
        />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/@vime/core@^5/themes/light.css"
        />*/}
      </Head>
        <NextNProgress color="#dc2626" />
        <Header />
         <Component {...pageProps} />
        <Footer />
    </React.Fragment>
  )
}

export default MyApp

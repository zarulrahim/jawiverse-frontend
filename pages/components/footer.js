import Head from 'next/head'
import { useEffect, useState } from 'react'
import styles from '../../styles/Home.module.css'
import { useRouter } from 'next/router'

export default function Footer() {
  return (
    <footer className="bg-gray-900 p-8 text-center text-white border-0">
      <a
        href="https://twitter.com/user/jawispace"
        target="_blank"
        rel="noopener noreferrer"
      >
       Built with ❤️ by&nbsp;JAWISPACE.IO
       <br />
        {/* <span className={styles.logo}>
          <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
        </span> */}
      </a>
      <div className='mt-1 text-gray-500'>
        © 2022 Jawispace
      </div>
    </footer>
  )
}
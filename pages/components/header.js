import { observer } from "mobx-react-lite";
import { getSnapshot } from "mobx-state-tree";
import { route } from "next/dist/server/router";
import { Menu, Disclosure } from '@headlessui/react'
import Link from "next/link";
import { FiChevronRight, FiShare2, FiHeart, FiSearch, FiChevronUp, FiChevronDown, FiMenu, FiDelete, FiX } from 'react-icons/fi';
import { useRouter } from "next/router";
import { useEffect } from "react";
import { mstAuth } from "../../mobx";

const Header = observer((props) => {
  const { pathname, locale, locales, defaultLocale, asPath } = useRouter();
  // console.log("check router ===> ", pathname)

  useEffect(() => {
    if (getSnapshot(mstAuth)?.isLoggedIn) {
      mstAuth.getProfile();
      // console.log("check auth ==> ", getSnapshot(mstAuth)?.user)
    }
  }, [getSnapshot(mstAuth).isLoggedIn])

  const connectWallet = () => {
    mstAuth.signWeb3()
    .then((response) => {
      mstAuth.login(response)
    })
    // mstAuth.connectWallet()
    // .then((response) => {
      
    // })
    // .catch((error) => {
    //   alert(error)
    // })
  };

  const onLogout = () => {
    mstAuth.logout()
    .then((response) => {
      // console.log(response)
    })
  }

  return (
    <div>
      <nav
        className={`${
          pathname === "/" ||
          pathname === "/projects/rumi" ||
          pathname === "/watch/[main_slug]" ||
          pathname === "/watch/[main_slug]/[single_content_slug]" ||
          pathname === "/tokens" ||
          pathname === "/leaderboard"
            ? "z-50 absolute bg-gray-800 md:bg-transparent"
            : "bg-gray-800"
        } border-gray-200 px-4 md:px-10 py-4 md:py-6 w-full`}
      >
        <div className="max-w-7xl grid grid-cols-4 container flex justify-between items-center mx-auto">
          <div className="w-full">
            <a href="/">
              <img src="https://jawiverse-bucket.sgp1.digitaloceanspaces.com/jawispace-logo-light-no-bg.png" className="w-8 md:w-10" />
            </a>
          </div>
          <div className="w-full md:order-2 col-span-3 md:col-span-1 flex justify-end">
            {
              getSnapshot(mstAuth)?.isLoggedIn ? 
              (
              <Menu as="div" className="relative z-50">
                <Menu.Button className="flex items-center bg-red-700 hover:bg-red-800 font-medium rounded-lg  px-4 py-3 text-center mr-0 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700"><span className="text-white text-sm md:text-md">{mstAuth.shortWalletAddress(getSnapshot(mstAuth)?.user.eth_address)} </span><FiChevronDown className="ml-2 text-white text-lg font-bold" /></Menu.Button>
                <Menu.Items as="div" className="absolute bg-white mt-2 rounded-lg overflow-hidden w-full">
                  <Menu.Item as="div" className="flex">
                    {({ active }) => (
                      <a
                        className={`${active && 'bg-gray-200'} text-sm w-full px-3 py-2 cursor-pointer`}
                        onClick={() => { alert("We are currently renovating your dashboard with interesting features. Stay tuned!")}}
                      >
                        Dashboard
                      </a>
                    )}
                  </Menu.Item>
                  <Menu.Item as="div" className="flex">
                    {({ active }) => (
                      <a
                        className={`${active && 'bg-gray-200'} text-sm w-full px-3 py-2 cursor-pointer`}
                        onClick={() => { onLogout() }}
                      >
                        Logout
                      </a>
                    )}
                  </Menu.Item>
                </Menu.Items>
              </Menu>
              )
              :
              (
                <button
                  onClick={connectWallet}
                  type="button"
                  className="text-white flex items-center md:text-md text-sm bg-red-700 hover:bg-red-800 font-medium rounded-lg  px-4 py-2.5 text-center mr-0 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700"
                >
                  <img src="https://raw.githubusercontent.com/MetaMask/brand-resources/c3c894bb8c460a2e9f47c07f6ef32e234190a7aa/SVG/metamask-fox.svg" className="w-6 h-6 mr-2" />
                  <span className="hidden md:inline-block">{locale === "en" ? "Connect Wallet" : "Sambung Wallet"}</span>
                  <span className="md:hidden">{locale === "en" ? "Connect" : "Sambung"}</span>
                </button>
              ) 
            }
            {/* <button
              data-collapse-toggle="mobile-menu-4"
              type="button"
              className="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
              aria-controls="mobile-menu-4"
              aria-expanded="false"
            >
              <span className="sr-only">Open main menu</span>
              <svg
                className="w-6 h-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <svg
                className="hidden w-6 h-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </button> */}
          </div>
          <div
            className="hidden col-span-2 justify-center items-center w-full md:flex md:w-auto md:order-1"
            id="mobile-menu-4"
          >
            <ul className="flex flex-col mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium">
              <li>
                <Link href={pathname === "/" ? "#rumi" : "/#rumi"}>
                  <span
                    className={`text-lg block py-2 pr-4 pl-3 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 cursor-pointer ${
                      pathname == "/projects/rumi"
                        ? "text-white dark:text-white"
                        : "text-gray-700 dark:text-gray-400"
                    }`}
                  >
                    RUMI: 3D Animated Series
                  </span>
                </Link>
              </li>
              <li>
                <Link href={pathname === "/" ? "#benefits" : "/#benefits"}>
                  <span
                    className={`text-lg block py-2 pr-4 pl-3 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 cursor-pointer ${
                      pathname == "/benefits"
                        ? "text-white dark:text-white"
                        : "text-gray-700 dark:text-gray-400"
                    }`}
                  >
                    Benefits
                  </span>
                </Link>
              </li>
              <li>
                <Link href={pathname === "/" ? "#nfts" : "/#nfts"}>
                  <span
                    className={`text-lg block py-2 pr-4 pl-3 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 cursor-pointer ${
                      pathname == "/nfts"
                        ? "text-white dark:text-white"
                        : "text-gray-700 dark:text-gray-400"
                    }`}
                  >
                    NFTs
                  </span>
                </Link>
              </li>
              <li>
                <Link href={pathname === "/" ? "#teams" : "/#teams"}>
                  <span
                    className={`text-lg block py-2 pr-4 pl-3 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 cursor-pointer ${
                      pathname == "/teams"
                        ? "text-white dark:text-white"
                        : "text-gray-700 dark:text-gray-400"
                    }`}
                  >
                    Teams
                  </span>
                </Link>
              </li>
              <li>
                <Link href={pathname === "/" ? "#faqs" : "/#faqs"}>
                  <span
                    className={`text-lg block py-2 pr-4 pl-3 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 cursor-pointer ${
                      pathname == "/faqs"
                        ? "text-white dark:text-white"
                        : "text-gray-700 dark:text-gray-400"
                    }`}
                  >
                    FAQs
                  </span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      {/* <nav className="md:hidden fixed z-50 w-full items-center grid grid-cols-4 bg-white dark:bg-gray-600">
        <div className="w-full mx-auto">
          <Disclosure>
            {({ open }) => (
              <>
              <Disclosure.Button>
                {
                  open ?
                  (
                    <FiX className="text-3xl text-white" />
                  )
                  :
                  (
                    <FiMenu className="text-3xl text-white" />
                  )
                }
              </Disclosure.Button>
              {
                open ?
                (
                  <Disclosure.Panel as="div" className="absolute mt- w-full">
                    <div className="w-full bg-white">
                      <div>RUMI</div>
                      <div>Benefits</div>
                      <div>Benefits</div>
                      <div>Benefits</div>
                      <div>Benefits</div>
                    </div>
                  </Disclosure.Panel>
                )
                :
                (
                  null
                )
              }
            </>
            )}
          </Disclosure>
        </div>
        <div className="w-full col-span-2 flex justify-center">
          <a href="/">
            <img src="https://jawiverse-bucket.sgp1.digitaloceanspaces.com/jawispace-logo-light-no-bg.png" className="w-10" />
          </a>
        </div>
        <div className="w-full">
          <a href="/">
            <img src="https://jawiverse-bucket.sgp1.digitaloceanspaces.com/jawispace-logo-light-no-bg.png" className="w-10" />
          </a>
        </div>
            </nav> */}
    </div>
  )
})
export default Header;
import Head from 'next/head'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
// import styles from '../styles/Home.module.css'
// import Header from "./components/Header"
import axios from 'axios';
import Web3 from 'web3';
// import '/public/pentasSmartContractAbi.json'
// import pentasContractAbi from "./public/pentasSmartContractAbi.json";
// import { useKeenSlider } from "keen-slider/react"
import Slider from 'react-slick'
import { FiShare2, FiHeart } from 'react-icons/fi';
import ReactPlayer from 'react-player';
import { Tab } from '@headlessui/react'
import { useRouter } from 'next/router';
import { observer } from 'mobx-react-lite';
import { getSnapshot } from 'mobx-state-tree';
import { mstGeneral } from '../../../mobx';

function changeVideoSource(blob, videoElement) {
  var blobUrl = URL.createObjectURL(blob);
  console.log(`Changing video source to blob URL "${blobUrl}"`)
  videoElement.src = blobUrl;
  videoElement.play();
}

function fetchVideo(url) {
  return fetch(url).then(function(response) {        
    return response.blob();
  });
}

function Arrow(props) {
  const disabeld = props.disabled ? " arrow--disabled" : ""
  return (
    <svg
      onClick={props.onClick}
      className={`arrow ${
        props.left ? "arrow--left" : "arrow--right"
      } ${disabeld}`}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
    >
      {props.left && (
        <path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z" />
      )}
      {!props.left && (
        <path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z" />
      )}
    </svg>
  )
}

const faqs = [
  {
    title: "What is JAWISPACE ?",
    content: `JAWISPACE is a Metaverse where anyone can do whatever they wanted to do while completing quests and gaining rewards in order to reach a higher level.`
  },
  {
    title: 'Is JAWISPACE produces 3D animated series?',
    content: `Yes. We are currently developing and producing a few 3D animated series projects such as <b>RUMI</b>.`
  },
  {
    title: 'How JAWISPACE sells the NFTs?',
    content: `Every projects that we produce will have so called "Access Pass" and "PFP" token. So, you can buy our access pass to watch our content and PFP to own our assets that we've developed and featured in our 3D animated series.`
  },
  {
    title: 'What is the chosen NFT marketplace?',
    content: `We are currently selling our NFTs in OpenSea marketplace and minting through the Polygon Blockchain. We do have a future execution to sell our NFTs in Pentas.io where it will be minted through Binance Smart Chain (BSC).`
  }
];

const teams = [
  {
    name: "Hafiy",
    twitter: "hafi_y",
    designation: "Founder",
    words: "Determination is key!",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Kcg Bijau",
    twitter: "kcg_bijau",
    designation: "Co-Founder",
    words: "Practice makes perfect!",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Itoi",
    twitter: "ito_i",
    designation: "Artist",
    words: "Believe in everything you do!",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Dayah",
    twitter: "dayah_nft",
    designation: "Copywriter",
    words: "",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Tasha",
    twitter: "tasha_nft",
    designation: "Social Media",
    words: "",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
];

const nfts = [
  {
    name: "Hafiy",
    twitter: "hafi_y",
    designation: "Founder",
    words: "Determination is key!",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Kcg Bijau",
    twitter: "kcg_bijau",
    designation: "Co-Founder",
    words: "Practice makes perfect!",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Itoi",
    twitter: "ito_i",
    designation: "Artist",
    words: "Believe in everything you do!",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Amira",
    twitter: "amira_nft",
    designation: "Copywriter",
    words: "",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Amira",
    twitter: "amira_nft",
    designation: "Copywriter",
    words: "",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
  {
    name: "Amira",
    twitter: "amira_nft",
    designation: "Copywriter",
    words: "",
    imageUrl: "https://pbs.twimg.com/profile_images/1483389459295256577/YVQmT24r_400x400.jpg",
  },
];

const utilities = [
  {
    name: 'Competitive exchange rates',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.',
    icon: FiHeart,
  },
  {
    name: 'No hidden fees',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.',
    icon: FiHeart,
  },
  {
    name: 'Transfers are instant',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.',
    icon: FiHeart,
  },
  {
    name: 'Mobile notifications',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.',
    icon: FiHeart,
  },
  {
    name: 'Transfers are instant',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.',
    icon: FiHeart,
  },
  {
    name: 'Mobile notifications',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.',
    icon: FiHeart,
  },
]

const Content = observer((props) => {
  const { query, locale, locales, defaultLocale, asPath } = useRouter();
  // const [videoUrl, setVideoUrl] = useState("")
  const [title, setTitle] = useState("jawiverse.io")
  const [currentSlide, setCurrentSlide] = useState(0)
  const [loaded, setLoaded] = useState(false)
  // const [showData, setShowData] = useState(null)
  // const [isLoading, setIsLoading] = useState(false)
  // const [sliderRef, instanceRef] = useKeenSlider({
  //   initial: 0,
  //   slideChanged(slider) {
  //     setCurrentSlide(slider.track.details.rel)
  //   },
  //   created() {
  //     setLoaded(true)
  //   },
  //   breakpoints: {
  //     "(min-width: 400px)": {
  //       slides: { perView: 2, spacing: 5 },
  //     },
  //     "(min-width: 1000px)": {
  //       slides: { perView: 5, spacing: 20 },
  //     },
  //   },
  //   slides: { perView: 1 },
  // })

  const convertedVideoSrc = async (stringUrl) => {
    // var URL = window.URL || window.webkitURL;
    let file = await fetch(stringUrl).then(r => r.blob());
    let url = URL.createObjectURL(file);
    return url;
    // return blob[0];
    // axios.get(stringUrl, {
    //   responseType: 'arraybuffer',
    //   headers: {
    //     'Content-Type': 'application/json'
    //   }
    // })
    // .then((response) => {
    //   console.log("check response ===> ", response)
    // })
    // console.log("check stringUrl ===> ", stringUrl)
  }

  useEffect(() => {
    // script for url to blob conversion
    // fetchVideo('https://media.vimejs.com/720p.mp4').then(function(blob) {
    //   var blobUrl = URL.createObjectURL(blob);
    //   setVideoUrl(blobUrl)
    //   console.log(`Changing video source to blob URL "${blobUrl}"`)
    // });
    // script for vime.js cdn
    const script = document.createElement("script");
    script.src = "https://cdn.jsdelivr.net/npm/@vime/core@^5/dist/vime/vime.esm.js";
    script.async = true;
    script.type = "module";
    document.body.appendChild(script);
  }, [])

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  };

  const characterSliderSettings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          // infinite: true,
          // dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          // arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  }

  const characters = [
    {
      id: 1,
      name: "Rumi",
      type: 'Protagonist',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/rumcomp_00000.png"
    },
    {
      id: 2,
      name: "Potron",
      type: 'Protagonist',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/potroncomp_00000.png"
    },
    {
      id: 3,
      name: "Ling",
      type: 'Antagonist',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/lingcomp_00000.png"
    },
    {
      id: 4,
      name: "Raz",
      type: 'Villain',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/raz-comp.png"
    }
  ]

  const bannerSliderSettings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  }

  // const onClickFaqs = id => {
  //   const array = selectedFaqs.slice()
  //   const found = array?.find(faq => faq === id)
  //   if (found) {
  //     setSelectedFaqs(current => current?.filter(faq => faq !== id))
  //   } else {
  //     setSelectedFaqs(current => current?.push(id))
  //   }
  //   console.log("check id ==> ", id, "check selectedFaqs ==> ", selectedFaqs)
  // }
  // let selectedAccount;
  // let pentasContract;
  // let currentBalance;
  // let isInitialized = false;
  // const init = async () => {
  //   let provider = window.ethereum;
  //   if (typeof provider !== 'undefined') {
  //     provider
  //       .request({ method: 'eth_requestAccounts' })
  //       .then((accounts) => {
  //         selectedAccount = accounts[0];
  //         console.log(`Selected account is ${selectedAccount}`);
  //       })
  //       .catch((err) => {
  //         console.log(err);
  //         return;
  //       });

  //     window.ethereum.on('accountsChanged', function (accounts) {
  //       selectedAccount = accounts[0];
  //       console.log(`Selected account changed to ${selectedAccount}`);
  //     });
  //   }

  //   const web3 = new Web3(provider);
  //   const networkId = await web3.eth.net.getId();

  //   const src = "/pentasSmartContractAbi.json"
  //   const response = await axios.get(src)
  //   console.log("check abi contract ===> ", JSON.parse(response.data.result))
  //   const pentasContractAbi = JSON.parse(response.data.result)

  //   pentasContract = new web3.eth.Contract(
  //     pentasContractAbi,
  //     "0x3aFa102B264b5f79ce80FED29E0724F922Ba57c7"
  //   );
  //   // const tokenIds = [185752, 185742];
  //   // tokenIds.map((id) => {
  //   //   pentasContract.methods.
  //   // })
  //   // pentasContract.methods.ownerOf(185752)
  //   // .call()
  //   // .then((address) => {
  //   //   console.log("Check owner of the token 185752 ===> ", address)
  //   // })
  //   pentasContract.methods.balanceOf(selectedAccount)
  //   .call()
  //   .then((balance) => {
  //     console.log("Check balance of the address ===> ", selectedAccount, balance)
  //     for (var i = 0; i < balance; i++) {
  //       pentasContract.methods.tokenOfOwnerByIndex(selectedAccount, i)
  //       .call()
  //       .then((id) => {
  //         console.log("check token id ===> ", i, id)
  //         pentasContract.methods.tokenURI(id)
  //         .call()
  //         .then((token) => {
  //           console.log("check token uri ===> ", i, token)
  //         })
  //       })
  //     }
  //   })
  //   isInitialized = true;
  // }

  useEffect(() => {
    mstGeneral.fetchSingleShow()
    mstGeneral.fetchTokens()
    // init()
  }, [])

  const FaqItem = (props) => {
    const [expandedFaq, setExpandedFaq] = useState(false)

    return (
      <div className='px-4 mb-4'>
        <a className="cursor-pointer" onClick={() => { setExpandedFaq(!expandedFaq) }}>
          <div className='w-full rounded-lg bg-red-600 flex justify-between p-4'>
            <div className='text-white'>{props.item.title}</div>
          </div>
        </a>
        <div className={`${expandedFaq ? `` : `hidden` } mt-4 mb-8 px-4 text-white`}>
          <div dangerouslySetInnerHTML={{__html: props.item.content }} />
        </div>
      </div>
    )
  };

  const TeamItem = (props) => {
    return (
      <div className="mr-2 ml-2 text-center">
        <div className="flex">
          <img className="w-32 h-32 mx-auto rounded-full border border-4 border-white" src={props.item.imageUrl} alt={props.item.name} />
        </div>
        <div className="text-sm mt-4">
          <p className="text-2xl text-white leading-none">{props.item.name}</p>
          <p className="text-gray-400 mt-2">{props.item.designation}</p>
          <p className="text-white mt-1"><a className="text-red-500 hover:text-red-600" href={`https://twitter.com/${props.item.twitter}`} target="_blank" rel="noreferrer">@{props.item.twitter}</a></p>
          {/* <p className="text-gray-400 mt-1">{props.item.words}</p> */}
        </div>
      </div>
    )
  };

  return (
    <div className='min-h-screen'>  
      {/* <div id="roadmap" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/top1.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-slate-900 bg-opacity-20 py-10 ">
          <div>
            <div className="text-center">
              <h1 className="text-white text-2xl md:text-5xl">Roadmap</h1>
              <h5 className="mt-4 text-gray-400 text-xl">Good roadmap delivers good outcome!</h5>
            </div>
            <div className='px-4 sm:px-10 mt-10 flex items-start'>
              
            </div>
          </div>
        </div>
      </div> */}
      <div id="rumi" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-main-rumi-jawispace-3000.jpg")' }}>
        <div className="flex items-start justify-center h-full w-full bg-gray-900 bg-opacity-95 py-10 ">
          <div className='w-full max-w-7xl pt-14 md:pt-20 px-4 md:px-0'>
            <div className='md:h-80 aspect-video overflow-hidden rounded-lg bg-cover' style={{ width: '100%', backgroundPosition: 'center center', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-main-rumi-jawispace-3000.jpg")' }}>
              
            </div>
            {/* <Slider {...bannerSliderSettings} className="overlayArrow rounded-2xl overflow-hidden h-80">
              {
                getSnapshot(mstGeneral)?.showData?.seasons?.length > 0 && getSnapshot(mstGeneral)?.showData?.seasons[0]?.episodes?.map((item, index) => {
                  return (
                    <div key={index}>
                      <div>
                        <img className="w-full" src={item?.poster_url} alt={item?.title} />
                      </div>
                    </div>
                  )
                })
              }
            </Slider> */}
            <div className='mt-5 flex justify-between items-center'>
              <div>
                <div className='md:flex items-center'>
                  <div><h1 className="text-white text-2xl md:text-4xl">The Adventure of <span className="font-bold text-red-600">{getSnapshot(mstGeneral)?.showData?.title}</span>  </h1></div>
                  <div className='flex mt-2 md:mt-0'>
                    <div className='bg-gray-600 text-white text-sm px-2 py-1 md:ml-5 rounded-md'>4 Episodes</div>
                    <div className='bg-gray-600 text-white text-sm px-2 py-1 ml-2 rounded-md'>Series</div>
                    {/* <div className='bg-orange-600 text-white text-sm px-2 py-1 ml-2 rounded-md'>Access Token (JACS) Required</div> */}
                  </div>
                </div>
              <h5 className="mt-2 text-gray-400 text-md max-w-4xl">{getSnapshot(mstGeneral)?.showData?.summary}</h5>
              </div>
              <div className='flex'>
                {/* <FiHeart className='text-white text-3xl' /> */}
                {/* <FiShare2 className='ml-5 text-white text-3xl' /> */}
              </div>
            </div>
            <div className='w-full mt-6 pb-10 border-b border-gray-200 dark:border-gray-700'>
              <Tab.Group>
                <Tab.List className="mb-4  border-b border-gray-200 dark:border-gray-700">
                  <Tab as={React.Fragment}>
                  {({ selected }) => (
                    <button
                      className={
                        `inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                      }
                    >
                      Episodes
                    </button>
                  )}
                  </Tab>
                  <Tab as={React.Fragment}>
                  {({ selected }) => (
                    <button
                      className={
                        `hidden md:inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                      }
                    >
                      Description
                    </button>
                  )}
                  </Tab>
                  <Tab as={React.Fragment}>
                  {({ selected }) => (
                    <button
                      className={
                        `inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                      }
                    >
                      Characters
                    </button>
                  )}
                  </Tab>
                  <Tab as={React.Fragment}>
                  {({ selected }) => (
                    <button
                      className={
                        `inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                      }
                    >
                      Credits
                    </button>
                  )}
                  </Tab>
                </Tab.List>
                <Tab.Panels>
                  <Tab.Panel>
                    <div className="flex grid grid-cols-1 md:grid-cols-4 gap-4" id="first" role="tabpanel" aria-labelledby="first-tab">
                    {
                      getSnapshot(mstGeneral)?.showData?.seasons?.length > 0 && getSnapshot(mstGeneral)?.showData?.seasons[0]?.episodes?.map((item, index) => {
                        return (
                          <div key={index}>
                            <div className={`max-w-full rounded-lg overflow-hidden`}>
                              <img className="rounded-lg aspect-video" src={item?.poster_url} alt="" />
                              <div className="py-3 h-50">
                                <div className="text-sm mb-0 text-white">Episode {index + 1}</div>
                                <div className="text-md mt-1 mb-0 text-white">{item?.title}</div>
                                <p className="text-gray-400 text-sm mt-1" style={{ height: '60px' }}>
                                  {item?.summary}
                                </p>
                              </div>
                              <div className='mt-5'>
                                {
                                  item?.external_url !== null ?
                                  (
                                    <Link href={`/watch/${query.main_slug}/${item?.slug}`}><div className="w-full text-white bg-red-600 hover:bg-red-700 font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700 cursor-pointer">Watch Now</div></Link>
                                  )
                                  :
                                  (
                                    <button href="/projects/rumi" disabled className="w-full text-white bg-gray-600 border border-gray-600  font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-gray-600">Coming Soon</button>
                                  )
                                }
                              </div>
                            </div>
                          </div>
                        )
                      })
                    }
                    </div>
                  </Tab.Panel>
                  <Tab.Panel>
                    <div className='mt-5'>
                      <div className='text-lg text-white border-l-8 rounded-md bg-gray-800 border-red-600 px-4 py-2 mb-5'>Synopsis</div>
                      <div className='text-md text-white max-w-4xl'>{getSnapshot(mstGeneral)?.showData?.summary}</div>
                    </div>
                    <div className='mt-5'>
                      <div className='text-lg text-white border-l-8 rounded-md bg-gray-800 border-red-600 px-4 py-2 mb-5'>Project Details</div>
                      <table className="table-auto">
                        <tbody>
                          <tr>
                            <td><div className='text-md text-white'>Title</div></td>
                            <td><div className='text-md text-white ml-10'>The Adventure of {getSnapshot(mstGeneral)?.showData?.title}</div></td>
                          </tr>
                          <tr>
                            <td><div className='text-md text-white'>Genre</div></td>
                            <td><div className='text-md text-white ml-10'>{getSnapshot(mstGeneral)?.showData?.genres?.map(g => g).join(', ')}</div></td>
                          </tr>
                          <tr>
                            <td><div className='text-md text-white'>Audio</div></td>
                            <td><div className='text-md text-white ml-10'>Bahasa Malaysia</div></td>
                          </tr>
                          <tr>
                            <td><div className='text-md text-white'>Subtitles</div></td>
                            <td><div className='text-md text-white ml-10'>English, Mandarin</div></td>
                          </tr>
                          <tr>
                            <td><div className='text-md text-white'>Episodes</div></td>
                            <td><div className='text-md text-white ml-10'>{getSnapshot(mstGeneral)?.showData?.seasons?.length > 0 ? getSnapshot(mstGeneral)?.showData?.seasons[0]?.episodes?.length : 0} Episodes (4 minutes per episode)</div></td>
                          </tr>
                        </tbody>
                      </table>
                    </div> 
                  </Tab.Panel>
                  <Tab.Panel>
                    <div className="w-full px-4" id="second" role="tabpanel" aria-labelledby="second-tab">
                      <Slider {...characterSliderSettings} className="w-full">
                      {
                        characters.map((item, index) => {
                          return (
                            <div key={index} className={`${index !== characters.length - 1 ? 'p-3' : 'p-3'}`}>
                              <div className={`max-w-full rounded-lg overflow-hidden`}>
                                <div className='flex justify-center'>
                                  <img className="w-56" src={item?.image_url} alt={item?.name} />
                                </div>
                                <div className="text-center px-4 py-2 mt-2">
                                  <div className=" text-white text-xl mb-0">{item?.name}</div>
                                  <div className=" text-gray-500 text-md mb-0">{item?.type}</div>
                                  {/* <p className="text-gray-700 text-base">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                                    </p>*/}
                                </div>
                              </div>
                            </div>
                          )
                        })
                      }
                      </Slider>
                    </div>
                  </Tab.Panel>
                  <Tab.Panel>
                    <div className="w-full" id="third" role="tabpanel" aria-labelledby="third-tab">
                      <div className='max-w-3xl w-full flex mx-auto items-start mt-5 grid grid-cols-1 md:grid-cols-3'>
                        <div>
                          <div className='text-center'>
                            <div className='text-md text-gray-400'>Directed by</div>
                            <div>
                              <div className='text-md text-white'>Hafiy</div>
                            </div>
                          </div>
                        </div>
                        <div className='mt-2 md:mt-0'>
                          <div className='text-center'>
                            <div className='text-md text-gray-400'>Concept & Benchmark</div>
                            <div>
                              <div className='text-md text-white'>Ezzul</div>
                            </div>
                          </div>
                        </div>
                        <div className='mt-2 md:mt-0'>
                          <div className='text-center'>
                            <div className='text-md text-gray-400'>Scriptwriter</div>
                            <div>
                              <div className='text-md text-white'>Atiqah</div>
                            </div>
                          </div>
                        
                        </div>
                      </div>
                      <div className='max-w-3xl w-full flex mx-auto items-start mt-2 md:mt-10 grid grid-cols-1 md:grid-cols-3'>
                        <div className='text-center'>
                          <div className='text-md text-gray-400'>Cast</div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Amirul Arif <span className="text-gray-400">as</span> Rumi</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Yasmeen <span className="text-gray-400">as</span> Ling</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Amirul Arif <span className="text-gray-400">as</span> Potron</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>TBA <span className="text-gray-400">as</span> Raz</div>
                          </div>
                        </div>
                        <div className='text-center mt-2 md:mt-0'>
                          <div className='text-md text-gray-400'>Crew</div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Adam <span className="text-gray-400">as</span> Storyboard</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Fahmie <span className="text-gray-400">as</span> Compositor</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Hafizul <span className="text-gray-400">as</span> Compositor</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Haizeel <span className="text-gray-400">as</span> Sketch FX</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Hakim <span className="text-gray-400">as</span> Animator</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Syafiq <span className="text-gray-400">as</span> Animator</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Azeril <span className="text-gray-400">as</span> Modeller</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Nazib <span className="text-gray-400">as</span> Rigger</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Rais <span className="text-gray-400">as</span> Modeller</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Nazmeen <span className="text-gray-400">as</span> Audio Designer</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Afiq <span className="text-gray-400">as</span> Music Composer</div>
                          </div>
                        </div>
                        <div className='text-center mt-4 md:mt-0'>
                          <div className='text-md text-gray-400'>Supported By</div>
                          <div className='flex justify-center mx-auto mt-0 md:mt-2'>
                            <img src="https://blog.mdec.my/wp-content/uploads/2017/10/mdec-logo-white.png" width={'50%'} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </Tab.Panel>
                </Tab.Panels>
              </Tab.Group>
            </div>
            <div className='w-full mt-5'>
              <div className='text-2xl text-white'>Related NFTs</div>
                <div className='mt-5'>
                  <Slider {...settings} className="w-full">
                    {
                      getSnapshot(mstGeneral)?.tokens?.map((item, index) => {
                        return (
                          <div key={index} className={`${index !== getSnapshot(mstGeneral)?.tokens?.length - 1 ? 'p-3' : 'p-3'}`}>
                            <div className={`rounded-lg overflow-hidden max-w-full shadow-lg bg-white`}>
                              <img className="w-full" src={item?.poster_url} alt="" />
                              <div className="px-4 py-2">
                                <div className="text-md mb-0 line-clamp-1">{item?.title}</div>
                                {/* <p className="text-gray-700 text-base">
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                                  </p>*/}
                              </div>
                              <div className="px-4 pt-0 pb-2">
                                <span className="inline-block bg-gray-200 rounded-lg px-2 py-1 text-xs text-gray-700 mr-2 mb-1">{item?.token_type}</span>
                              </div>
                            </div>
                          </div>
                        )
                      })
                    }
                  </Slider>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
})

export default Content;

import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import { FiShare2, FiHeart, FiPlayCircle } from 'react-icons/fi';
import { Tab } from '@headlessui/react'
import { useRouter } from 'next/router';
import { observer } from 'mobx-react-lite';
import { getSnapshot } from 'mobx-state-tree';
import { mstAuth, mstGeneral } from '../../../../mobx';
import { Player, Hls, DefaultUi } from '@vime/react';
import Slider from 'react-slick';
import dynamic from 'next/dynamic'

const SingleContent = observer((props) => {
  const { query, locale, locales, defaultLocale, asPath } = useRouter();
  const [episodeData, setEpisodeData] = useState(null)
  const [walletVerified, setWalletVerified] = useState(false)
  const [contentUrl, setContentUrl] = useState("")
  
  const VideoPlayer = dynamic(() => import('../../../plugins/player'), {
    ssr: false,
  })

  const verifyContentAccess = () => {
    if (getSnapshot(mstAuth)?.isLoggedIn) {
      mstAuth.signWeb3()
      .then((web3Response) => {
        const params = {
          ...web3Response,
          content: {
            type: 'show',
            id: episodeData?.id
          }
        }
        mstAuth.verifyContentAccess(params)
        .then((response) => {
          setContentUrl(response?.content_url);
          setWalletVerified(true)
        })
        .catch((error) => {
          setContentUrl("");
          setWalletVerified(false);
        })
      })
      .catch((error) => {
        alert(error);
      })
    } else {
      alert("Please connect wallet")
    }
  };
  
  useEffect(() => {
    if (query?.single_content_slug !== undefined) {
      mstGeneral.fetchSingleShow();
      mstGeneral.fetchTokens();
      const params = {
        season: getSnapshot(mstGeneral)?.showData?.seasons?.length > 0 && getSnapshot(mstGeneral)?.showData?.seasons[0]?.id,
        show: getSnapshot(mstGeneral)?.showData?.id
      }
      console.log("query.single_content_slug ===> ", query?.single_content_slug)
      mstGeneral.fetchSingleEpisode(params, query.single_content_slug)
      .then((response) => {
        setEpisodeData(response)
      })
      .catch((error) => {
        alert(error)
      })
    }
  }, [query?.single_content_slug])

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  };

  const characterSliderSettings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          // infinite: true,
          // dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          // arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  }

  const characters = [
    {
      id: 1,
      name: "Rumi",
      type: 'Protagonist',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/rumcomp_00000.png"
    },
    {
      id: 2,
      name: "Potron",
      type: 'Protagonist',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/potroncomp_00000.png"
    },
    {
      id: 3,
      name: "Ling",
      type: 'Protagonist',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/lingcomp_00000.png"
    },
    {
      id: 4,
      name: "Raz",
      type: 'Villain',
      image_url: "https://jawiverse-bucket.sgp1.digitaloceanspaces.com/shows/rumi/characters/raz-comp.png"
    }
  ]

  return (
    <div className='min-h-screen'>  
      {/* <div id="roadmap" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/top1.png")' }}>
        <div className="flex items-start justify-center h-full w-full bg-slate-900 bg-opacity-20 py-10 ">
          <div>
            <div className="text-center">
              <h1 className="text-white text-2xl md:text-5xl">Roadmap</h1>
              <h5 className="mt-4 text-gray-400 text-xl">Good roadmap delivers good outcome!</h5>
            </div>
            <div className='px-4 sm:px-10 mt-10 flex items-start'>
              
            </div>
          </div>
        </div>
      </div> */}
      <div id="rumi" className="-z-50 bg-cover bg-center bg-fixed" style={{ width: '100%', backgroundPosition: 'center bottom', backgroundImage: 'url("https://jawiverse-bucket.sgp1.digitaloceanspaces.com/bg-main-rumi-jawispace-3000.jpg")' }}>
        <div className="flex items-start justify-center h-full w-full bg-gray-900 bg-opacity-95 py-10 ">
          <div className='w-full max-w-7xl mt-14 md:mt-20 px-4 md:px-0'>
            <div className='rounded-lg overflow-hidden'>
              <VideoPlayer
                mediaType="youtube"
                poster={episodeData?.poster_url}
                videoSrc={episodeData?.external_url}
              />
              {/*
                getSnapshot(mstAuth)?.isLoggedIn && walletVerified ?
                (
                  <VideoPlayer
                    poster={episodeData?.poster_url}
                    videoSrc={episodeData?.video_url}
                  />
                )
                :
                (
                  <div className='w-full aspect-video md:h-screen bg-cover bg-center' style={{ backgroundImage: `url(${episodeData?.poster_url})` }}>
                    <div className='flex h-full items-center justify-center bg-gray-900 bg-opacity-30'>
                      <a onClick={() => { verifyContentAccess() }} className="w-20 h-20 flex justify-center items-center text-white text-opacity-80 hover:text-opacity-100 font-medium rounded-full text-6xl text-center cursor-pointer"><FiPlayCircle /></a>
                    </div>
                  </div>
                )
              */}
            </div>
            <div className='mt-6 flex justify-between items-center'>
              <div>
                <h5 className="text-white text-md">Episode {episodeData?.priority_no}</h5>
                <h1 className="mt-1 text-white text-2xl md:text-2xl break-normal">The Adventure of <span className="font-bold text-red-600">{getSnapshot(mstGeneral)?.showData?.title}</span> <span className='mt-1 md:mt-0 flex md:inline-block'><span className='hidden md:inline-block'>-</span> {episodeData?.title}</span></h1>
                <h5 className="mt-1 text-gray-400 text-md max-w-4xl">{episodeData?.summary}</h5>
              </div>
              <div className='flex'>
                {/* <FiHeart className='text-white text-3xl' />
                <FiShare2 className='ml-5 text-white text-3xl' /> */}
              </div>
            </div>
            <div className='w-full mt-6 pb-10 border-b border-gray-200 dark:border-gray-700'>
              <Tab.Group>
                <Tab.List className="mb-4 border-b border-gray-200 dark:border-gray-700">
                  <Tab as={React.Fragment}>
                  {({ selected }) => (
                    <button
                      className={
                        `inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                      }
                    >
                      Episodes
                    </button>
                  )}
                  </Tab>
                  <Tab as={React.Fragment}>
                  {({ selected }) => (
                    <button
                      className={
                        `hidden md:inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                      }
                    >
                      Description
                    </button>
                  )}
                  </Tab>
                  <Tab as={React.Fragment}>
                  {({ selected }) => (
                    <button
                      className={
                        `inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                      }
                    >
                      Characters
                    </button>
                  )}
                  </Tab>
                  <Tab as={React.Fragment}>
                  {({ selected }) => (
                    <button
                      className={
                        `inline-block py-4 px-4 text-sm font-medium text-center text-gray-500 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-gray-300 dark:text-gray-400 dark:hover:text-gray-300 ${selected ? 'active' : ''}`
                      }
                    >
                      Credits
                    </button>
                  )}
                  </Tab>
                </Tab.List>
                <Tab.Panels>
                  <Tab.Panel>
                    <div className="flex grid grid-cols-1 md:grid-cols-4 gap-4" id="first" role="tabpanel" aria-labelledby="first-tab">
                    {
                      getSnapshot(mstGeneral)?.showData?.seasons?.length > 0 && getSnapshot(mstGeneral)?.showData?.seasons[0]?.episodes?.map((item, index) => {
                        return (
                          <div key={index}>
                            <div className={`max-w-full rounded-lg overflow-hidden`}>
                              <img className="rounded-lg aspect-video" src={item?.poster_url} alt="" />
                              <div className="py-3 h-50">
                                <div className="text-sm mb-0 text-white">Episode {index + 1}</div>
                                <div className="text-md mt-1 mb-0 text-white">{item?.title}</div>
                                <p className="text-gray-400 text-sm mt-1" style={{ height: '60px' }}>
                                  {item?.summary}
                                </p>
                              </div>
                              <div className='mt-5'>
                                {
                                  item?.external_url !== null ?
                                  (
                                    <Link href={`/watch/${query.main_slug}/${item?.slug}`}><div className="w-full text-white bg-red-600 hover:bg-red-700 font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-red-600 dark:hover:bg-red-700 cursor-pointer">Watch Now</div></Link>
                                  )
                                  :
                                  (
                                    <button href="/projects/rumi" disabled className="w-full text-white bg-gray-600 border border-gray-600  font-medium rounded-lg text-md px-5 py-3 text-center mr-3 md:mr-0 dark:bg-gray-600">Coming Soon</button>
                                  )
                                }
                              </div>
                            </div>
                          </div>
                        )
                      })
                    }
                    </div>
                  </Tab.Panel>
                  <Tab.Panel>
                    <div className='mt-5'>
                      <div className='text-lg text-white border-l-8 rounded-md bg-gray-800 border-red-600 px-4 py-2 mb-5'>Synopsis</div>
                      <div className='text-md text-white max-w-4xl'>{getSnapshot(mstGeneral)?.showData?.summary}</div>
                    </div>
                    <div className='mt-5'>
                      <div className='text-lg text-white border-l-8 rounded-md bg-gray-800 border-red-600 px-4 py-2 mb-5'>Project Details</div>
                      <table className="table-auto">
                        <tbody>
                          <tr>
                            <td><div className='text-md text-white'>Title</div></td>
                            <td><div className='text-md text-white ml-10'>The Adventure of {getSnapshot(mstGeneral)?.showData?.title}</div></td>
                          </tr>
                          <tr>
                            <td><div className='text-md text-white'>Genre</div></td>
                            <td><div className='text-md text-white ml-10'>{getSnapshot(mstGeneral)?.showData?.genres?.map(g => g).join(', ')}</div></td>
                          </tr>
                          <tr>
                            <td><div className='text-md text-white'>Audio</div></td>
                            <td><div className='text-md text-white ml-10'>Bahasa Malaysia</div></td>
                          </tr>
                          <tr>
                            <td><div className='text-md text-white'>Subtitles</div></td>
                            <td><div className='text-md text-white ml-10'>English, Mandarin</div></td>
                          </tr>
                          <tr>
                            <td><div className='text-md text-white'>Episodes</div></td>
                            <td><div className='text-md text-white ml-10'>{getSnapshot(mstGeneral)?.showData?.seasons?.length > 0 ? getSnapshot(mstGeneral)?.showData?.seasons[0]?.episodes?.length : 0} Episodes (4 minutes per episode)</div></td>
                          </tr>
                        </tbody>
                      </table>
                    </div> 
                  </Tab.Panel>
                  <Tab.Panel>
                    <div className="w-full px-4" id="second" role="tabpanel" aria-labelledby="second-tab">
                      <Slider {...characterSliderSettings} className="w-full">
                      {
                        characters.map((item, index) => {
                          return (
                            <div key={index} className={`${index !== characters.length - 1 ? 'p-3' : 'p-3'}`}>
                              <div className={`max-w-full rounded-lg overflow-hidden`}>
                                <div className='flex justify-center'>
                                  <img className="w-56" src={item?.image_url} alt={item?.name} />
                                </div>
                                <div className="text-center px-4 py-2 mt-2">
                                  <div className=" text-white text-xl mb-0">{item?.name}</div>
                                  <div className=" text-gray-500 text-md mb-0">{item?.type}</div>
                                  {/* <p className="text-gray-700 text-base">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                                    </p>*/}
                                </div>
                              </div>
                            </div>
                          )
                        })
                      }
                      </Slider>
                    </div>
                  </Tab.Panel>
                  <Tab.Panel>
                    <div className="w-full" id="third" role="tabpanel" aria-labelledby="third-tab">
                      <div className='max-w-3xl w-full flex mx-auto items-start mt-5 grid grid-cols-1 md:grid-cols-3'>
                        <div>
                          <div className='text-center'>
                            <div className='text-md text-gray-400'>Directed by</div>
                            <div>
                              <div className='text-md text-white'>Hafiy</div>
                            </div>
                          </div>
                        </div>
                        <div className='mt-2 md:mt-0'>
                          <div className='text-center'>
                            <div className='text-md text-gray-400'>Concept & Benchmark</div>
                            <div>
                              <div className='text-md text-white'>Ezzul</div>
                            </div>
                          </div>
                        </div>
                        <div className='mt-2 md:mt-0'>
                          <div className='text-center'>
                            <div className='text-md text-gray-400'>Scriptwriter</div>
                            <div>
                              <div className='text-md text-white'>Atiqah</div>
                            </div>
                          </div>
                        
                        </div>
                      </div>
                      <div className='max-w-3xl w-full flex mx-auto items-start mt-2 md:mt-10 grid grid-cols-1 md:grid-cols-3'>
                        <div className='text-center'>
                          <div className='text-md text-gray-400'>Cast</div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Amirul Arif <span className="text-gray-400">as</span> Rumi</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Yasmeen <span className="text-gray-400">as</span> Ling</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Amirul Arif <span className="text-gray-400">as</span> Potron</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>TBA <span className="text-gray-400">as</span> Raz</div>
                          </div>
                        </div>
                        <div className='text-center mt-2 md:mt-0'>
                          <div className='text-md text-gray-400'>Crew</div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Adam <span className="text-gray-400">as</span> Storyboard</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Fahmie <span className="text-gray-400">as</span> Compositor</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Hafizul <span className="text-gray-400">as</span> Compositor</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Haizeel <span className="text-gray-400">as</span> Sketch FX</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Hakim <span className="text-gray-400">as</span> Animator</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Syafiq <span className="text-gray-400">as</span> Animator</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Azeril <span className="text-gray-400">as</span> Modeller</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Nazib <span className="text-gray-400">as</span> Rigger</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Rais <span className="text-gray-400">as</span> Modeller</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Nazmeen <span className="text-gray-400">as</span> Audio Designer</div>
                          </div>
                          <div className='flex justify-center'>
                            <div className='text-md text-white'>Afiq <span className="text-gray-400">as</span> Music Composer</div>
                          </div>
                        </div>
                        <div className='text-center mt-4 md:mt-0'>
                          <div className='text-md text-gray-400'>Supported By</div>
                          <div className='flex justify-center mx-auto mt-0 md:mt-2'>
                            <img src="https://blog.mdec.my/wp-content/uploads/2017/10/mdec-logo-white.png" width={'50%'} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </Tab.Panel>
                </Tab.Panels>
              </Tab.Group>
            </div>
            <div className='w-full mt-5'>
              <div className='text-2xl text-white'>Related NFTs</div>
                <div className='mt-5'>
                  <Slider {...settings} className="w-full">
                    {
                      getSnapshot(mstGeneral)?.tokens?.map((item, index) => {
                        return (
                          <div key={index} className={`${index !== getSnapshot(mstGeneral)?.tokens?.length - 1 ? 'p-3' : 'p-3'}`}>
                            <div className={`rounded-lg overflow-hidden max-w-full shadow-lg bg-white`}>
                              <img className="w-full" src={item?.poster_url} alt="" />
                              <div className="px-4 py-2">
                                <div className="text-md mb-0 line-clamp-1">{item?.title}</div>
                                {/* <p className="text-gray-700 text-base">
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                                  </p>*/}
                              </div>
                              <div className="px-4 pt-0 pb-2">
                                <span className="inline-block bg-gray-200 rounded-lg px-2 py-1 text-xs text-gray-700 mr-2 mb-1">{item?.token_type}</span>
                              </div>
                            </div>
                          </div>
                        )
                      })
                    }
                  </Slider>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
})

export default SingleContent;

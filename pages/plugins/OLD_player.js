import { Player as VimePlayer, Hls, DefaultUi } from '@vime/react'

const Player = (props) => (
  <VimePlayer theme="dark">
    <Hls crossOrigin poster={props.poster}>
      <source
        data-src={props.videoSrc}
        type="application/x-mpegURL"
      />
    </Hls>
    <DefaultUi />
  </VimePlayer>
)

export default Player
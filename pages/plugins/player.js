import { Player as VimePlayer, Youtube, Hls, DefaultUi, Ui, Controls, PlaybackControl, ControlSpacer, MuteControl, Scrim, TimeProgress } from '@vime/react'

const Player = (props) => (
  <VimePlayer theme="dark">
    {
      props.mediaType && props.mediaType === 'youtube' ?
      (
        <Youtube videoId={props.videoSrc} />
      )
      :
      (
        <Hls crossOrigin poster={props.poster}>
          <source
            data-src={props.videoSrc}
            type="application/x-mpegURL"
          />
        </Hls>
      )
    }
    {
      props.type === 'simpleUi' ?
      (
        <Ui>
          <Scrim />
          <Controls fullWidth pin="topLeft">
            <ControlSpacer />
            <MuteControl />
          </Controls>

          <Controls pin="center">
            <PlaybackControl hideTooltip style={{ '--vm-control-scale': 1.7 }} />
          </Controls>

          <Controls fullWidth pin="bottomLeft">
            <ControlSpacer />
            <TimeProgress />
          </Controls>
        </Ui>
      )
      :
      (
        <DefaultUi />
      )
    }
  </VimePlayer>
)

export default Player
const { i18n } = require('./next-i18next.config');
const withTM = require('next-transpile-modules')(['@vime/core', '@vime/react']);
module.exports = withTM({
  reactStrictMode: true,
  images: {
    domains: ['jawiverse-bucket.sgp1.digitaloceanspaces.com'],
  },
  distDir: 'build',
  trailingSlash: true,
  i18n,
})
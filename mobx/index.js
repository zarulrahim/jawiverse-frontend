// import { localStorage } from 'react';
import { types, flow, getSnapshot } from "mobx-state-tree";
import axios from 'axios';
import persist from 'mst-persist';
import Web3 from 'web3';

const BASE_URL = "https://console.jawispace.io"
const DEFAULT_SHOW_SLUG = "rumi"

const FetchApi = (path, parameters, id) => {
  // console.log("fetcapi check path, parameters, id ==> ", path, parameters, id)
  let query = "";
  let url = BASE_URL + '/api/' 
  if (parameters !== undefined) {
    for (let key in parameters) {
      let value = parameters[key];
      query += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
    }
  }
  if (query.length > 0) {
    query = query.substring(0, query.length - 1);
    if (id !== undefined) {
      url = url + path + "/" + id + "?" + query;
    } else {
      url = url + path + "?" + query;
    }
  } else {
    if (id !== undefined) {
      url = url + path + "/" + id
    } else {
      url = url + path
    }
  }
  // console.log("check url sat ===> ", url)
  return url;
}

// const FetchApiNoParam = (path, id) => {
//   let url = BASE_URL + '/api/'
//   if (id !== undefined) {
//     url = url + path + "/" + id
//   } else {
//     url = url + path
//   }
//   return url;
// }

const General = types.model({
  showData: types.optional(types.map(types.frozen()), {}),
  tokens: types.optional(types.array(types.frozen()), []),
  holder_leaderboard: types.optional(types.array(types.frozen()), []),
}) 
.actions(self => {
  const fetchSingleShow = flow (function * () {
    try {
      const url = FetchApi("shows", undefined, DEFAULT_SHOW_SLUG)
      // console.log("fetchSingleShow url ===> ", url)
      const response = yield axios.get(url)
      if (response.data) {
        // console.log("fetchSingleShow response ===> ", response.data)
        self.showData = response.data
        return Promise.resolve(response.data)
      } else {
        // console.log("fetchSingleShow invalid response ===> ", response.data)
        return Promise.reject(response.data)
      }
    } catch (err) {
      // console.log("fetchSingleShow catch error ===> ", err)
      return Promise.reject(err)
    }
  })
  const fetchSingleEpisode = flow (function * (parameters, slug) {
    try {
      // console.log("check slug ===> ", slug)
      const url = FetchApi("episodes", parameters, slug)
      // console.log("fetchSingleEpisode url ===> ", url)
      const response = yield axios.get(url)
      if (response.data) {
        // console.log("fetchSingleEpisode response ===> ", response.data)
        return Promise.resolve(response.data)
      } else {
        // console.log("fetchSingleEpisode invalid response ===> ", response.data)
        return Promise.reject(response.data)
      }
    } catch (err) {
      // console.log("fetchSingleEpisode catch error ===> ", err)
      return Promise.reject(err)
    }
  })
  const fetchTokens = flow (function * (parameters) {
    try {
      const url = FetchApi("tokens", parameters, undefined)
      // console.log("fetchTokens url ===> ", url)
      const response = yield axios.get(url)
      if (response.data) {
        // console.log("fetchTokens response ===> ", response.data)
        self.tokens = response.data
        return Promise.resolve(response.data)
      } else {
        // console.log("fetchTokens invalid response ===> ", response.data)
        return Promise.reject(response.data)
      }
    } catch (err) {
      // console.log("fetchTokens catch error ===> ", err)
      return Promise.reject(err)
    }
  })
  const fetchLeaderboard = flow (function * (parameters) {
    try {
      const url = FetchApi("holder_leaderboard", parameters, undefined)
      // console.log("fetchLeaderboard url ===> ", url)
      const response = yield axios.get(url)
      if (response.data) {
        // console.log("fetchLeaderboard response ===> ", response.data)
        self.holder_leaderboard = response.data
        return Promise.resolve(response.data)
      } else {
        // console.log("fetchLeaderboard invalid response ===> ", response.data)
        return Promise.reject(response.data)
      }
    } catch (err) {
      // console.log("fetchLeaderboard catch error ===> ", err)
      return Promise.reject(err)
    }
  })
  return { fetchSingleShow, fetchSingleEpisode, fetchTokens, fetchLeaderboard }
})
export const mstGeneral = General.create({})

const Auth = types.model({
  isLoggedIn: types.optional(types.boolean, false),
  user: types.optional(types.map(types.frozen()), {}),
  token: types.optional(types.string, ""),
})
.actions(self => {
  const signWeb3 = flow (function * () {
    if (typeof window !== 'undefined') {
      const web3 = new Web3(window.ethereum);
      if (typeof web3 !== 'undefined') {
        // console.log('web3 is enabled')
        if (web3.currentProvider.isMetaMask === true) {
          const networkType = yield web3.eth.net.getNetworkType()
          const chainId = yield web3.eth.net.getId() // require 56 BSC Network
          // console.log("network type ===> ", networkType)
          // console.log("chain id ===> ", chainId)
          if (chainId !== 56) {
            alert('Please change to BSC network')
          } else {
            // yield window.ethereum.send('eth_requestAccounts');
            const address = (yield web3.eth.requestAccounts())[0]
            const message = [
              address,
              'https://jawispace.io'
            ].join("\n")
            const signature = yield web3.eth.personal.sign(message, address)
            const params = { address, message, signature }
            // console.log("check params ===> ", params)
            return Promise.resolve(params)
          }
        } else {
          alert("Please install MetaMask extension to proceed");
        }
      } else {
        alert("Please use Chrome browser or install MetaMask app and use their in-app browser to proceed");
      }
    }    
  })
  const login = flow (function * (parameters) {
    try {
      const url = FetchApi("login", undefined, undefined)
      const response = yield axios.post(url, parameters);
      // console.log("check response ===> ", response)
      if (response.status === 200) {
        // console.log("mobx login success ==> ", response)
        self.isLoggedIn = true
        self.user = response.data.user
        self.token = response.data.token
        return Promise.resolve(response)
      } else {
        // console.log("mobx login error ===> ", response)
        return Promise.reject(response)
      }
      // self.signWeb3()
      // .then((response) => {
      //   const url = FetchApi("login", undefined, undefined)
      //   const loginResponse = await axios.post(url, response);
      //   console.log("check loginResponse ===> ", loginResponse)
      //   if (loginResponse.data) {
      //     console.log("mobx login success ==> ", loginResponse)
      //     self.isLoggedIn = true
      //     return Promise.resolve(loginResponse)
      //   } else {
      //     console.log("mobx login error ===> ", loginResponse)
      //     return Promise.reject(loginResponse)
      //   }
        // axios.post(url, response)
        // .then((response) => {
        //   console.log("mobx login success ==> ", response)
        //   self.isLoggedIn = true
        //   return Promise.resolve(response)
        // })
        // .catch((error) => {
        //   console.log("mobx login error ===> ", error)
        //   return Promise.reject(error)
        // })
      // })
    } catch (err) {
      // console.log("login catch error ===> ", err)
      return Promise.reject(err)
    }
  })
  const logout = flow (function * () {
    try {
      const url = FetchApi("logout", undefined, undefined)
      const config = {
        headers: { Authorization: `Bearer ${self.token}` }
      };
      const response = yield axios.post(url, null, config)
      if (response.status === 200) {
        self.isLoggedIn = false
        self.user = {}
        self.token = ""
        // console.log("mobx logout response  ===> ", response)
        return Promise.resolve(response.data)
      } else {
        self.isLoggedIn = false
        self.user = {}
        self.token = ""
        // console.log("mobx logout error ===> ", error)
        return Promise.reject(error)
      }
    } catch (err) {
      self.isLoggedIn = false
      self.user = {}
      self.token = ""
      // console.log("mobx logout error ===> ", err)
      return Promise.reject(err)
    }
  })
  const getProfile = flow (function * () {
    try {
      const url = FetchApi("profile", undefined, undefined)
      const config = {
        headers: { Authorization: `Bearer ${self.token}` }
      };
      const response = yield axios.get(url, config)
      if (response.status === 200) {
        self.isLoggedIn = true
        self.user = response.data.user
        // console.log("mobx getProfile response  ===> ", response)
        return Promise.resolve(response.data)
      } else {
        self.isLoggedIn = false
        self.user = {}
        // console.log("mobx getProfile error ===> ", response.data)
        return Promise.reject(response.data)
      }
    } catch (err) {
      self.isLoggedIn = false
      self.user = {}
      // console.log("mobx getProfile error ===> ", err)
      return Promise.reject(err)
    }
  })
  const verifyContentAccess = flow (function * (parameters) {
    // console.log("verifycontentaccess params ===> ", parameters)
    try {
      const url = FetchApi("verify_content_access", undefined, undefined)
      const config = {
        headers: { Authorization: `Bearer ${self.token}` }
      };
      const response = yield axios.post(url, parameters, config)
      if (response.data.status === 'success') {
        // console.log("mobx getProfile response  ===> ", response)
        return Promise.resolve(response.data)
      } else {
        // console.log("mobx getProfile error ===> ", response.data)
        return Promise.reject(response.data)
      }
    } catch (err) {
      // console.log("mobx getProfile error ===> ", err)
      return Promise.reject(err)
    }
  })
  return { signWeb3, login, logout, getProfile, verifyContentAccess }
})
.views(self => ({
  shortWalletAddress(address) {
    return address.length > 12 ? address.substring(0, 6) + '...' + address.substring(address.length - 4, address.length)  : address
  }
}))
export const mstAuth = Auth.create({})

if (typeof window !== 'undefined') { // window is undefined in Node
  persist('mstGeneral', mstGeneral, { storage: window.localStorage, jsonify: true, }) //.then(() => console.log('mstGeneral has been persisted'))
  persist('mstAuth', mstAuth, { storage: window.localStorage, jsonify: true, }) //.then(() => console.log('mstAuth has been persisted'))
}